package com.filip.fitapp.Enum;

public enum ZahtevStatus {
    PRIHVACEN,
    ODBIJEN,
    NA_CEKANJU
}
