package com.filip.fitapp.Enum;

public enum StatusKomentara {
    NA_CEKANJU,
    ODOBREN,
    NIJE_ODOBREN
}
