package com.filip.fitapp.Kontroler;

import com.filip.fitapp.Model.Korpa;
import com.filip.fitapp.Model.Termin;
import com.filip.fitapp.Model.Trening;
import com.filip.fitapp.Service.PopustService;
import com.filip.fitapp.Service.TerminService;
import com.filip.fitapp.Service.TreningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class KorpaKontroler {

    @Autowired
    TerminService terminService;

    @Autowired
    PopustService popustService;

    @Autowired
    TreningService treningService;

    @GetMapping(value="/Korpa")
    public ModelAndView korpa(HttpSession session) {
        ModelAndView page = new ModelAndView("korpa");
        page.addObject("korpa", session.getAttribute("korpa"));
        return page;
    }

    @PostMapping(value = "/Korpa")
    public ModelAndView korpa(@RequestParam int idTermin, HttpSession session) {

        ModelAndView page = new ModelAndView("korpa");
        Termin termin = terminService.findTerminById(idTermin);
        page.addObject("termin", termin);

        Trening trening = null;
        for (Trening tr: popustService.setPopust()) {
            if (tr.getId() == termin.getTreningid()) {
                trening = tr;
            }
        }

        page.addObject("trening", trening);

        Korpa korpa = (Korpa) session.getAttribute("korpa");

        korpa.getTerminTreningMap().put(termin, trening);
        page.addObject("korpa", korpa);

        return page;
    }

    @GetMapping(value="/Korpa/RemoveItem")
    public ModelAndView korpaRemoveItem(@RequestParam int id, HttpSession session) {

        ModelAndView page = new ModelAndView("korpa");
        Korpa korpa = (Korpa) session.getAttribute("korpa");

        for (Map.Entry<Termin, Trening> entry : korpa.getTerminTreningMap().entrySet()) {
            if (entry.getKey().getId() == id) {
                korpa.getTerminTreningMap().remove(entry.getKey());
            }
        }

        page.addObject("korpa", korpa);
        page.setViewName("redirect:/Korpa");
        return page;
    }

    @PostMapping(value = "/Korpa/Checkout")
    public ModelAndView checkout(HttpSession session) {

        session.setAttribute("ulogovanClan", "true");
        int korId = (int) session.getAttribute("korId");
        session.setAttribute("korId", korId);

        Korpa korpa = (Korpa) session.getAttribute("korpa");

        if (korpa.getTerminTreningMap().isEmpty()) {

            ModelAndView page = new ModelAndView("korpa");
            page.addObject("korpa", session.getAttribute("korpa"));
            String msg = "Nemate nista u korpi - please add something to cart";
            page.addObject("msg", msg);
            return page;
        }
        boolean preklapanje = false;

        for (Termin t1: korpa.getTerminTreningMap().keySet()) {
            for (Termin t2: korpa.getTerminTreningMap().keySet()) {
                if (t1 == t2) {
                    continue;
                }
                Calendar cal = Calendar.getInstance();
                cal.setTime(t1.getDatum());
                cal.add(Calendar.HOUR_OF_DAY, treningService.findTreningById(t1.getTreningid()).getTrajanje());
                Date kraj1 = cal.getTime();

                Calendar cal2= Calendar.getInstance();
                cal2.setTime(t2.getDatum());
                cal2.add(Calendar.HOUR_OF_DAY, treningService.findTreningById(t2.getTreningid()).getTrajanje());
                Date kraj2 = cal2.getTime();

                if (t2.getDatum().before(kraj1) && t1.getDatum().before(kraj2)) {
                    preklapanje = true;
                }
            }
        }

        if (preklapanje) {

            ModelAndView page = new ModelAndView("korpa");
            page.addObject("korpa", session.getAttribute("korpa"));
            page.addObject("msg", "mozete samo jedan trening u ovo vreme");
            return page;

        } else {
            for (Termin t3: korpa.getTerminTreningMap().keySet()) {
                Termin termin = terminService.findTerminById(t3.getId());
                terminService.updateTermin(termin.getId(), termin.getPopunjen() + 1);
                terminService.addKorisnik(korId, termin.getId());
            }
            session.removeAttribute("korpa");

            Random random = new Random();
            int idRandom = random.nextInt(89999) + 10000;

            Map<Termin, Trening> map = new HashMap<>();
            Korpa newKorpa = new Korpa(idRandom, korId, map);
            session.setAttribute("korpa", newKorpa);

            ModelAndView page = new ModelAndView("ulogovaniClan");
            page.addObject("treninzi", popustService.setPopust());
            session.setAttribute("porukaPostoji", "true");
            session.setAttribute("poruka", "hvala na kupovini - thanks for purchase");
            page.setViewName("redirect:/Clan");
            return page;
        }
    }
}
