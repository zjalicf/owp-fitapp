package com.filip.fitapp.Kontroler;

import com.filip.fitapp.Enum.StatusKomentara;
import com.filip.fitapp.Enum.ZahtevStatus;
import com.filip.fitapp.Model.Komentar;
import com.filip.fitapp.Model.Zahtev;
import com.filip.fitapp.Service.CardService;
import com.filip.fitapp.Service.KorisnikService;
import com.filip.fitapp.Service.TipService;
import com.filip.fitapp.Service.TreningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class ClanskaKarticaKontroler {

    @Autowired
    TreningService treningService;

    @Autowired
    KorisnikService korisnikService;

    @Autowired
    TipService tipService;

    @Autowired
    CardService cardService;

    @GetMapping(value = "/FitCard")
    public ModelAndView cardGet(HttpSession session) {

        ModelAndView page = new ModelAndView("fitcard");
        page.addObject("ulogovaniClan", "true");

        int korId = (int) session.getAttribute("korId");
        List<Zahtev> zahtevi = cardService.getZahtevi();
        if (zahtevi.isEmpty()) {
            page.addObject("vecPoslao", "false");
        } else {
            for (Zahtev zahtev : zahtevi) {
                if (zahtev.getKorId() == korId) {
                    page.addObject("vecPoslao", "true");
                    page.addObject("msg", "vec ste poslali zahtev - already submitted");
                } else {
                    page.addObject("vecPoslao", "false");
                }
            }
        }
        return page;
    }

    @PostMapping(value = "/FitCard")
    public ModelAndView cardPost(HttpSession session) {

        int korId = (int) session.getAttribute("korId");
        cardService.posaljiZahtev(korId);

        ModelAndView page = new ModelAndView("ulogovaniClan");

        page.addObject("treninzi", treningService.findAllTrening());
        page.addObject("tipovi", tipService.findAll());
        page.addObject("msg", "poslat zahtev - request for card sent");
        return page;
    }

    @GetMapping("/Admin/Zahtevi")
    public ModelAndView zahteviGet(HttpSession session) {

        ModelAndView page = new ModelAndView("zahteviCard");
        page.addObject("zahtevi", cardService.getZahtevi());
        return page;
    }

    @PostMapping("Admin/Zahtevi")
    public ModelAndView zahteviPost(@RequestParam(value = "odbij", required = false) String odbij,
                                    @RequestParam(value = "odobri", required = false) String odobri,
                                    @RequestParam(value = "id") Integer id,
                                    HttpSession session) {

        ModelAndView page= new ModelAndView("ulogovaniAdmin");
        Zahtev zahtev = cardService.findZahtev(id);
        if (odbij != null) {
            zahtev.setZahtevStatus(ZahtevStatus.ODBIJEN);
            cardService.updateZahtev(zahtev);
            page.addObject("treninzi", treningService.findAllTrening());
            page.setViewName("redirect:/Admin");
            return page;

        } else if (odobri != null){
            zahtev.setZahtevStatus(ZahtevStatus.PRIHVACEN);
            cardService.updateZahtev(zahtev);

            cardService.createCard((int) session.getAttribute("korId"));
            page.addObject("treninzi", treningService.findAllTrening());
            page.setViewName("redirect:/Admin");
            return page;

        } else {
            return new ModelAndView("greska");
        }
    }

    @PostMapping("/Admin/OdbijZahtev")
    public ModelAndView komentariOdbij(@RequestParam(value = "id", required = false) Integer id, HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {
            ModelAndView page = new ModelAndView("info");
//            Zahtev zahtev = cardService.findZahtev(id);
//            zahtev.setZahtevStatus(ZahtevStatus.ODBIJEN);
//            cardService.updateZahtev(zahtev);
            return page;
        } else {
            return new ModelAndView("greska");
        }
    }

    @PostMapping("/Admin/OdobriZahtev")
    public ModelAndView komentariOdobri(@RequestParam(value = "id", required = false) Integer id, HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {
            ModelAndView page = new ModelAndView("info");
            Zahtev zahtev = cardService.findZahtev(id);
            zahtev.setZahtevStatus(ZahtevStatus.PRIHVACEN);
            cardService.updateZahtev(zahtev);

            cardService.createCard((int) session.getAttribute("korId"));
            return page;
        } else {
            return new ModelAndView("greska");
        }
    }
}
