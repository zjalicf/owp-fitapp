package com.filip.fitapp.Kontroler;

import com.filip.fitapp.Enum.NivoTreninga;
import com.filip.fitapp.Enum.Trener;
import com.filip.fitapp.Enum.Uloga;
import com.filip.fitapp.Enum.VrstaTreninga;
import com.filip.fitapp.Model.*;
import com.filip.fitapp.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller
public class KorisnikKontroler {

    public static final String KORISNIK_KEY = "korisnik";

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private IzvestajService izvestajService;

    @Autowired
    private TipService tipService;

    @Autowired
    private TreningService treningService;

    @Autowired
    private TerminService terminService;

    @Autowired
    private PopustService popustService;

    @Autowired
    private KorisnikTerminService korisnikTerminService;

    @GetMapping(value = "/Login")
    public ModelAndView loginPage() {
        return new ModelAndView("login");
    }

    @PostMapping(value = "/Login")
    public ModelAndView postLogin(@RequestParam String korIme,
                                  @RequestParam String sifra,
                                  HttpSession session) {
        try {
            Korisnik korisnik = korisnikService.findKorisnik(korIme, sifra);
            if (korisnik == null) {
                throw new Exception("Pogresno korisnicko ime ili lozinka");
            } else if (korisnik.isBlokiran()) {
                throw new Exception("Vas nalog je blokiran");
            }

            if (Uloga.ADMIN.equals(korisnik.getUloga())) {
                session.setAttribute("ulogovanAdmin", "true");
                session.setAttribute("korId", korisnik.getId());

                ModelAndView page = new ModelAndView("ulogovaniAdmin");
                page.addObject("treninzi", treningService.findAllTrening());
                page.setViewName("redirect:/Admin");
                return page;
            }

            if (Uloga.CLAN.equals(korisnik.getUloga())) {
                session.setAttribute("ulogovanClan", "true");
                session.setAttribute("korId", korisnik.getId());

                Random random = new Random();
                int idRandom = random.nextInt(89999) + 10000;
                Map<Termin, Trening> map = new HashMap<>();
                Korpa korpa = new Korpa(idRandom, korisnik.getId(), map);
                session.setAttribute("korpa", korpa);

                popustService.setPopust();

                ModelAndView page = new ModelAndView("ulogovaniClan");

                page.addObject("treninzi", popustService.setPopust());
                page.setViewName("redirect:/Clan");
                return page;
            }
            return new ModelAndView("greska");

        } catch (Exception ex) {
            String msg = ex.getMessage();
            if (msg == null || msg.isEmpty()) {
                msg = "Greska - error";
            }

            ModelAndView err = new ModelAndView("login");
            err.addObject("msg", msg);
            return err;
        }
    }

    @GetMapping(value = "/Register")
    public ModelAndView registerPage() {
        return new ModelAndView("register");
    }

    @PostMapping(value = "/Register")
    public ModelAndView register(@RequestParam String korIme,
                                 @RequestParam String sifra,
                                 @RequestParam String email,
                                 @RequestParam String ime,
                                 @RequestParam String prezime,
                                 @RequestParam String datumRodjenja,
                                 @RequestParam String adresa,
                                 @RequestParam int brojTelefona,
                                 @RequestParam String sifra2) {
        try {
            Korisnik alreadyExists = korisnikService.findKorisnikUsername(korIme);
            if (alreadyExists != null) {
                throw new Exception("Korisnik sa takvim korisnickim imenom postoji");
            }
            if (korIme.equals("") || sifra.equals("")) {
                throw new Exception("Unesite korisnicko ime i sifru");
            }
            if (!sifra2.equals(sifra)) {
                throw new Exception("Sifre se ne podudaraju");
            }
            if (email.equals("")) { // provera za mail
                throw new Exception("Morate uneti ispravan email");
            }
            if (ime.equals("")) {
                throw new Exception("Morate uneti ime");
            }
            if (prezime.equals("")) {
                throw new Exception("Morate uneti prezime");
            }
            if (adresa.equals("")) {
                throw new Exception("Morate uneti adresu");
            }
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate datumRodjenjaFixed = LocalDate.parse(datumRodjenja, formatter);
            if (datumRodjenjaFixed.isAfter(LocalDate.now())) {
                throw new Exception("Unesi tacan datum rodjenja");
            }
            if (String.valueOf(brojTelefona).length() == 0) {
                throw new Exception("Morate uneti broj telefona");
            }

            Random random = new Random();
            int id = random.nextInt(8999) + 1000;
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date datumRodjFix = format.parse(datumRodjenja);

            Korisnik korisnik = new Korisnik(id, korIme, sifra, email, ime, prezime, datumRodjFix, adresa,
                    brojTelefona, new Date(), Uloga.CLAN, "N");

            korisnikService.saveKorisnik(korisnik);
            return new ModelAndView("login");

        } catch (Exception ex) {
            String msg = ex.getMessage();
            if (Objects.equals(msg, "")) {
                msg = "Greska - error";
            }

            ModelAndView res = new ModelAndView("register");
            res.addObject("msg", msg);

            return res;
        }
    }

    @GetMapping(value = "/Clan")
    public ModelAndView clanPage(HttpSession session) {
        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanClan"))) {

            ModelAndView page = new ModelAndView("ulogovaniClan");

            if("true".equals(session.getAttribute("porukaPostoji"))) {
                page.addObject("msg", session.getAttribute("poruka"));
                session.setAttribute("porukaPostoji", "false");
                session.setAttribute("poruka", "");
            }

            page.addObject("treninzi", popustService.setPopust());
            page.addObject("tipovi", tipService.findAll());
            return page;
        }
        return new ModelAndView("greska");
    }

    @PostMapping(value = "/Clan")
    public ModelAndView clanPage(@RequestParam(name = "naziv", required = false) String naziv,
                                  @RequestParam(name = "tipTrDropdown", required = false) String tipovi,
                                  @RequestParam(name = "cenaMin", required = false) Double cenaMin,
                                  @RequestParam(name = "cenaMax", required = false) Double cenaMax,
                                  @RequestParam(name = "trenerDropDown", required = false) Trener trener,
                                  @RequestParam(name = "vrstaTrDropDown", required = false) VrstaTreninga vrsta,
                                  @RequestParam(name = "nivoTrDropDown", required = false) NivoTreninga nivo,
                                  @RequestParam(name = "sort", required = false) String sort,
                                  @RequestParam(name = "order", required = false) String order, HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanClan"))) {

            ModelAndView page = new ModelAndView("ulogovaniClan");

            page.addObject("treninzi", treningService.findTrening(naziv, cenaMin, cenaMax, tipovi, trener, vrsta , nivo, sort, order));
            page.addObject("tipovi", tipService.findAll());
            return page;
        }
        return new ModelAndView("greska");
    }

    @GetMapping(value = "/Profil")
    public ModelAndView profilClanaPage(HttpSession session) {

        ModelAndView page = new ModelAndView("mojProfil");

        if("true".equals(session.getAttribute("porukaPostoji"))) {
            page.addObject("msg", session.getAttribute("poruka"));
            session.setAttribute("porukaPostoji", "false");
            session.setAttribute("poruka", "");
        }

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanClan"))) {
            page.addObject("ulogovanClan", "true");
            return page;

        } else if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))){
            page.addObject("ulogovanAdmin", "true");
            return page;

        } else {
            return new ModelAndView("greska");
        }
    }

    @GetMapping(value = "/Admin")
    public ModelAndView adminPage(HttpSession session) {
        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {
            ModelAndView page = new ModelAndView("ulogovaniAdmin");
            page.addObject("treninzi", treningService.findAllTrening());
            page.addObject("tipovi", tipService.findAll());
            return page;
        }
        return new ModelAndView("greska");
    }

    @PostMapping(value = "/Admin")
    public ModelAndView adminPage(@RequestParam(name = "naziv", required = false) String naziv,
                                  @RequestParam(name = "tipTrDropdown", required = false) String tipovi,
                                  @RequestParam(name = "cenaMin", required = false) Double cenaMin,
                                  @RequestParam(name = "cenaMax", required = false) Double cenaMax,
                                  @RequestParam(name = "trenerDropDown", required = false) Trener trener,
                                  @RequestParam(name = "vrstaTrDropDown", required = false) VrstaTreninga vrsta,
                                  @RequestParam(name = "nivoTrDropDown", required = false) NivoTreninga nivo,
                                  @RequestParam(name = "sort", required = false) String sort,
                                  @RequestParam(name = "order", required = false) String order, HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovan"))) {

            ModelAndView page = new ModelAndView("ulogovaniAdmin");
            page.addObject("treninzi", treningService.findTrening(naziv, cenaMin, cenaMax, tipovi, trener, vrsta , nivo, sort, order));
            page.addObject("tipovi", tipService.findAll());
            return page;
        }
        return new ModelAndView("greska");
    }

    @GetMapping(value = "Profil/IzmeniProfil")
    public ModelAndView izmeniProfilGet(HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanClan")) || Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {

            if (session.getAttribute("korId") != null) {
                int id = (int) session.getAttribute("korId");
                ModelAndView page = new ModelAndView("izmeniProfil");
                page.addObject("korisnik", korisnikService.findKorisnikById(id));
                return page;
            }
        }
        return new ModelAndView("greska");
    }

    @PostMapping(value = "Profil/IzmeniProfil")
    public ModelAndView izmeniProfilPost(@RequestParam(required = false) String korIme,
                                 @RequestParam(required = false) String sifra,
                                 @RequestParam(required = false) String email,
                                 @RequestParam(required = false) String ime,
                                 @RequestParam(required = false) String prezime,
                                 @RequestParam(required = false) String datumRodjenja,
                                 @RequestParam(required = false) String adresa,
                                 @RequestParam(required = false) int brojTelefona,
                                 @RequestParam(required = false) String sifra2, HttpSession session) {

        boolean sifraPromenjena = false;
        int id = 0;
        try {
            if (korIme.equals("")) {
                throw new Exception("Unesite korisnicko ime i sifru");
            }
            if (!sifra.equals("")) {
                sifraPromenjena = true;
            }
            if (!sifra2.equals(sifra)) {
                throw new Exception("Sifre se ne podudaraju");
            }
            if (email.equals("")) { // provera za mail
                throw new Exception("Morate uneti ispravan email");
            }
            if (ime.equals("")) {
                throw new Exception("Morate uneti ime");
            }
            if (prezime.equals("")) {
                throw new Exception("Morate uneti prezime");
            }
            if (adresa.equals("")) {
                throw new Exception("Morate uneti adresu");
            }

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate datumRodjenjaFixed = LocalDate.parse(datumRodjenja, formatter);
            if (datumRodjenjaFixed.isAfter(LocalDate.now())) {
                throw new Exception("Unesi tacan datum rodjenja");
            }
            if (String.valueOf(brojTelefona).length() == 0) {
                throw new Exception("Morate uneti broj telefona");
            }

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date datumRodjFix = format.parse(datumRodjenja);


            id = (int) session.getAttribute("korId");
            Korisnik korisnik = korisnikService.findKorisnikById(id);

            korisnik.setKorIme(korIme);
            if (sifraPromenjena) {
                korisnik.setSifra(sifra);
            }
            korisnik.setEmail(email);
            korisnik.setIme(ime);
            korisnik.setPrezime(prezime);
            korisnik.setDatumRodjenja(datumRodjFix);
            korisnik.setAdresa(adresa);
            korisnik.setBrojTelefona(brojTelefona);

            korisnikService.updateKorisnik(korisnik);
            ModelAndView res = new ModelAndView("mojProfil");
            session.setAttribute("porukaPostoji", "true");
            session.setAttribute("poruka", "Uspesno izmenjen profil - profile changed");
            res.setViewName("redirect:/Profil");
            return res;

        } catch (Exception ex) {
            String msg = ex.getMessage();
            if (Objects.equals(msg, "")) {
                msg = "Greska";
            }

            ModelAndView page = new ModelAndView("izmeniProfil");
            page.addObject("korisnik", korisnikService.findKorisnikById(id));
            page.addObject("msg", msg);

            return page;
        }
    }

    @GetMapping(value = "/Admin/Korisnici")
    public ModelAndView korisniciPage (HttpSession session) {

        ModelAndView page = new ModelAndView("korisnici");
        page.addObject("korisnici", korisnikService.findAll());
        return page;
    }

    @PostMapping(value = "/Admin/Korisnici")
    public ModelAndView korisniciPost(@RequestParam(name = "korIme", required = false) String korIme,
                                  @RequestParam(name = "ulogaDropdown", required = false) String uloga,
                                  @RequestParam(name = "sort", required = false) String sort,
                                  @RequestParam(name = "order", required = false) String order) {

        ModelAndView page = new ModelAndView("korisnici");
        page.addObject("korisnici", korisnikService.findFilter(korIme, uloga, sort, order));
        return page;
    }

    @GetMapping(value = "/Admin/Korisnik")
    public ModelAndView korisnikGet(@RequestParam(name = "id", required = false) Integer id, HttpSession session) {

        ModelAndView page = new ModelAndView("korisnik");
        page.addObject("korisnik", korisnikService.findKorisnikById(id));
        return page;

    }

    @PostMapping(value = "/Admin/Korisnik")
    public ModelAndView korisnikPost(@RequestParam(name = "btnBlokiran") String btnBlokiran,
                                     @RequestParam(name = "korId", required = false) Integer korId, HttpSession session) {

        ModelAndView page = new ModelAndView("korisnici");
        if ("blokiraj".equals(btnBlokiran)) {
            String msg = "Korisnik blokiran - User blocked";
            Korisnik korisnik = korisnikService.findKorisnikById(korId);
            korisnik.setBlokiran(true);
            korisnikService.updateKorisnik(korisnik);
            page.addObject("msg", msg);
            page.addObject("korisnici", korisnikService.findAll());
            return page;

        } else if ("odblokiraj".equals(btnBlokiran)) {
            String msg = "Korisnik odblokiran - user unblocked";
            Korisnik korisnik = korisnikService.findKorisnikById(korId);
            korisnik.setBlokiran(false);
            korisnikService.updateKorisnik(korisnik);
            page.addObject("msg", msg);
            page.addObject("korisnici", korisnikService.findAll());
            return page;

        } else {
            return new ModelAndView("greska");
        }
    }

    @GetMapping(value = "/Korisnik/Treninzi")
    public ModelAndView treninzi(HttpSession session) {

        int korId = (int) session.getAttribute("korId");
        ModelAndView page = new ModelAndView("korisnikTreninzi");

        Map<Trening, Termin> map = new LinkedHashMap<>();

        for (Termin termin: terminService.findUserTrainings(korId)) {
            map.put(treningService.findTreningById(termin.getTreningid()), termin);
        }

        page.addObject("map", map);
//        page.addObject("suma", );
        return page;
    }

    @GetMapping(value = "/Korisnik/Termin")
    public ModelAndView mojTerminGet(@RequestParam(name="id") Integer id) {

        ModelAndView page = new ModelAndView("terminPage");
        Termin termin = terminService.findTerminById(id);
        page.addObject("termin", termin);
        page.addObject("trening", treningService.findTreningById(termin.getTreningid()));
        return page;
    }

    @PostMapping(value = "/Korisnik/OtkaziTrening")
    public ModelAndView otkazi(@RequestParam(name = "id") Integer id, HttpSession session) {
        Date datum = terminService.findTerminById(id).getDatum();
        Calendar cal = Calendar.getInstance();
        cal.setTime(datum);
        cal.add(Calendar.DAY_OF_YEAR, -1);
        Date dayBefore = cal.getTime();
        Date sada = new Date();
        if (sada.before(dayBefore)) {
            int korId = (int) session.getAttribute("korId");
            korisnikTerminService.otkaziTermin(korId, id);
            session.setAttribute("porukaPostoji", "true");
            session.setAttribute("poruka", "uspeh - success");

        } else {
            session.setAttribute("porukaPostoji", "true");
            session.setAttribute("poruka", "ne moze - denied");
        }
        return new ModelAndView("redirect:/Profil");
    }

    @GetMapping(value={"/Logout", "/Admin/Logout"})
    public ModelAndView logout(HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanClan")) || Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {
        session.removeAttribute(KORISNIK_KEY);
        session.invalidate();

        ModelAndView page = new ModelAndView("homepage");
        page.addObject("treninzi", popustService.setPopust());
        page.setViewName("redirect:/");
        return page;
        } else {
            return new ModelAndView("greska");
        }
    }

    @GetMapping(value = "/Admin/Izvestaji")
    public ModelAndView izvestajiGet(HttpSession session) {

        ModelAndView page = new ModelAndView("izvestaji");
        List<Izvestaj> izvestaji = izvestajService.getIzvestaji();
        int ukupanBroj = 0;
        int ukupnaCena = 0;
        for (Izvestaj izvestaj: izvestaji) {
            ukupnaCena += izvestaj.getCena();
            ukupanBroj += izvestaj.getBrZakazanih();
        }
        page.addObject("izvestaji", izvestaji);
        page.addObject("ukupanBroj", ukupanBroj);
        page.addObject("ukupnaCena", ukupnaCena);
        return page;
    }

    @PostMapping(value = "/Admin/Izvestaji")
    public ModelAndView indexPage(@RequestParam(name = "cenaTreninga", required = false) Integer cena,
                                  @RequestParam(name = "datumOd", required = false) String datumOd,
                                  @RequestParam(name = "datumDo", required = false) String datumDo,
                                  @RequestParam(name = "brojZakazanih", required = false) Integer brZakazanih,
                                  @RequestParam(name = "sort", required = false) String sort,
                                  @RequestParam(name = "order", required = false) String order) throws ParseException {

        ModelAndView page = new ModelAndView("izvestaji");
        page.addObject("izvestaji", izvestajService.filterIzvestaji(datumOd, datumDo, brZakazanih, cena, sort, order));

        return page;
    }
}
