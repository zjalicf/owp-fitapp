package com.filip.fitapp.Kontroler;

import com.filip.fitapp.Enum.NivoTreninga;
import com.filip.fitapp.Enum.Trener;
import com.filip.fitapp.Enum.VrstaTreninga;
import com.filip.fitapp.Model.Popust;
import com.filip.fitapp.Model.Trening;
import com.filip.fitapp.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import java.util.List;

@Controller
@RequestMapping(value = "/")
public class MainKontroler {

    @Autowired
    private ServletContext servletContext;
    private String bURL;

    @Autowired
    TreningService treningService;

    @Autowired
    TipService tipService;

    @Autowired
    SalaService salaService;

    @Autowired
    PopustService popustService;

    @Autowired
    IzvestajService izvestajService;

    @PostConstruct
    public void init() {
        bURL = servletContext.getContextPath();
    }

    @GetMapping()
    public ModelAndView indexPage() {

        ModelAndView page = new ModelAndView("homepage");
        page.addObject("treninzi", popustService.setPopust());
        page.addObject("tipovi", tipService.findAll());
        return page;
    }

    @PostMapping()
    public ModelAndView indexPage(@RequestParam(name = "naziv", required = false) String naziv,
                                  @RequestParam(name = "tipTrDropdown", required = false) String tipovi,
                                  @RequestParam(name = "cenaMin", required = false) Double cenaMin,
                                  @RequestParam(name = "cenaMax", required = false) Double cenaMax,
                                  @RequestParam(name = "trenerDropDown", required = false) Trener trener,
                                  @RequestParam(name = "vrstaTrDropDown", required = false) VrstaTreninga vrsta,
                                  @RequestParam(name = "nivoTrDropDown", required = false) NivoTreninga nivo,
                                  @RequestParam(name = "sort", required = false) String sort,
                                  @RequestParam(name = "order", required = false) String order) {

        ModelAndView page = new ModelAndView("homepage");
        page.addObject("treninzi", treningService.findTrening(naziv, cenaMin, cenaMax, tipovi, trener, vrsta , nivo, sort, order));
        page.addObject("tipovi", tipService.findAll());
        return page;
    }
}
