package com.filip.fitapp.Kontroler;

import com.filip.fitapp.Enum.StatusKomentara;
import com.filip.fitapp.Model.Komentar;
import com.filip.fitapp.Model.Termin;
import com.filip.fitapp.Model.Trening;
import com.filip.fitapp.Service.KomentarService;
import com.filip.fitapp.Service.KorisnikService;
import com.filip.fitapp.Service.TerminService;
import com.filip.fitapp.Service.TreningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.method.HandlerTypePredicate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ThemeResolver;

import javax.servlet.http.HttpSession;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller
public class KomentarKontroler {

    @Autowired
    KorisnikService korisnikService;

    @Autowired
    TreningService treningService;

    @Autowired
    KomentarService komentarService;

    @PostMapping(value = "/DodajKomentar")
    public ModelAndView dodajKomentar(@RequestParam(name = "ocena") int ocena,
                                      @RequestParam(name = "tekst2") String tekst,
                                      @RequestParam(name = "treningId") int treningId,
                                      @RequestParam(name = "anon") String anon,
                                      HttpSession session) throws ParseException {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanClan"))) {

            ModelAndView page = new ModelAndView("info");

            Random random = new Random();
            int id = random.nextInt(899999) + 100000;
            int korId = (int) session.getAttribute("korId");
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date dateFixed = formatter.parse(LocalDate.now().toString());

            String anonStr = "N";
            if("true".equals(anon)) {
                anonStr = "Y";
            }
            Komentar komentar = new Komentar(id, tekst, ocena, dateFixed, korisnikService.findKorisnikById(korId),
                    treningService.findTreningById(treningId), StatusKomentara.NA_CEKANJU, anonStr);

            komentarService.saveKomentar(komentar);
            return page;
        }
        return new ModelAndView("greska");
    }


    @GetMapping(value = "/KomentariPending")
    public ModelAndView komentariPendingGet(HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {
            ModelAndView page = new ModelAndView("komentariAdmin");
            page.addObject("komentari", komentarService.findAllPending());
            return page;
        } else {
            return new ModelAndView("greska");
        }
    }

    @PostMapping(value = "/Admin/OdbijKomentar")
    public ModelAndView komentariOdbij(@RequestParam(value = "id", required = false) Integer id, HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {
            ModelAndView page = new ModelAndView("info");
            Komentar komentar = komentarService.findKomentarById(id);
            komentar.setStatusKomentara(StatusKomentara.NIJE_ODOBREN);
            komentarService.updateKomentar(komentar);
            return page;
        } else {
            return new ModelAndView("greska");
        }
    }

    @PostMapping(value = "/Admin/OdobriKomentar")
    public ModelAndView komentariOdobri(@RequestParam(value = "id") Integer id, HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {
            ModelAndView page = new ModelAndView("info");
            Komentar komentar = komentarService.findKomentarById(id);
            komentar.setStatusKomentara(StatusKomentara.ODOBREN);
            komentarService.updateKomentar(komentar);

            Trening trening = treningService.findTreningById(komentar.getTrening().getId());
            List<Komentar> komentari = komentarService.findAllCommentsForTraining(trening.getId());
            double novaOcena = komentar.getOcena();

            for (Komentar komentar1: komentari) {
                novaOcena+=komentar1.getOcena();
            }

            trening.setOcena(novaOcena/(komentari.size()+1));
            treningService.updateTrening(trening);
            return page;

        } else {
            return new ModelAndView("greska");
        }
    }
}
