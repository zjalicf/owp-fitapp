package com.filip.fitapp.Kontroler;

import com.filip.fitapp.Model.Sala;
import com.filip.fitapp.Model.Trening;
import com.filip.fitapp.Model.WishlistItem;
import com.filip.fitapp.Service.SalaService;
import com.filip.fitapp.Service.TerminService;
import com.filip.fitapp.Service.TreningService;
import com.filip.fitapp.Service.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
public class WishlistKontroler {

    @Autowired
    private WishlistService wishlistService;

    @Autowired
    private TreningService treningService;

    @GetMapping(value = "Profil/Wishlist")
    public ModelAndView profilClanaPage(HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanClan"))) {
            ModelAndView page = new ModelAndView("wishlist");
            int korId = (int) session.getAttribute("korId");

            List<Trening> treninzi = new ArrayList<>();
            for (WishlistItem item: wishlistService.getWishlistByUserId(korId)) {
                treninzi.add(treningService.findTreningById(item.getTreningId()));
            }
            page.addObject("treninzi", treninzi);
            return page;

        } else {
            return new ModelAndView("greska");
        }
    }

    @GetMapping(value = "/Wishlist/Dodaj")
    public ModelAndView wishlistDodaj(@RequestParam() Integer id, HttpSession session) {

        String msg = "fail";
        int korId = (int) session.getAttribute("korId");

        List<Integer> idList = new ArrayList<>();
        for (WishlistItem item: wishlistService.getWishlistByUserId(korId)) {
            idList.add(item.getTreningId());
        }

        if (!idList.contains(id)) {
            Random random = new Random();
            int idRandom = random.nextInt(899999) + 100000;

            WishlistItem wishlistItem = new WishlistItem(idRandom, korId, id);
            wishlistService.addItemToWishlist(wishlistItem);
            msg = "success";
        }

        ModelAndView page = new ModelAndView("info");
        page.addObject("msg", msg);
        return page;
    }

    @PostMapping(value = {"/RemoveWishlistItem", "/Profil/RemoveWishlistItem"})
    public ModelAndView removeWishlistItemPost(@RequestParam() Integer idTrening, HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanClan"))) {

            int korId = (int) session.getAttribute("korId");
            wishlistService.removeItem(korId, idTrening);
            ModelAndView page = new ModelAndView("mojProfil");


            List<Trening> treninzi = new ArrayList<>();
            for (WishlistItem item: wishlistService.getWishlistByUserId(korId)) {
                treninzi.add(treningService.findTreningById(item.getTreningId()));
            }
            page.addObject("treninzi", treninzi);
            page.setViewName("redirect:/Profil");
            return page;

        } else {
            return new ModelAndView("greska");
        }
    }
}
