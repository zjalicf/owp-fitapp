package com.filip.fitapp.Kontroler;

import com.filip.fitapp.Enum.NivoTreninga;
import com.filip.fitapp.Enum.Trener;
import com.filip.fitapp.Enum.VrstaTreninga;
import com.filip.fitapp.Model.Sala;
import com.filip.fitapp.Model.Termin;
import com.filip.fitapp.Model.Trening;
import com.filip.fitapp.Service.SalaService;
import com.filip.fitapp.Service.TerminService;
import com.filip.fitapp.Service.TipService;
import com.filip.fitapp.Service.TreningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Random;

@Controller
public class SalaKontroler {

    @Autowired
    SalaService salaService;

    @Autowired
    TreningService treningService;

    @Autowired
    TipService tipService;

    @Autowired
    TerminService terminService;

    @GetMapping(value = "/Admin/Sale")
    public ModelAndView index(HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {
            ModelAndView page = new ModelAndView("sale");
            page.addObject("sale", salaService.findAll());
            return page;
        } else {
            return new ModelAndView("greska");
        }
    }

    @PostMapping(value = "Admin/Sale")
    public ModelAndView indexPost(@RequestParam(name = "oznaka", required = false) String oznaka,
                                  @RequestParam(name = "sort", required = false) String kolona,
                                  @RequestParam(name = "order", required = false) String smer,
    @RequestParam(name="obrisiSalu", required = false) String obrisi,
                                  @RequestParam(name = "id") Integer id) {

        if (obrisi != null) {
            System.out.println("delete sala" + id); // pogresan id
            return new ModelAndView("sale").addObject("sale", salaService.findAll());
        }
        ModelAndView page = new ModelAndView("sale");
        page.addObject("sale", salaService.findAllFilter(oznaka, kolona, smer));
        return page;
    }

    @GetMapping(value = "Admin/IzmeniSalu")
    public ModelAndView izmeniGet(@RequestParam(name = "id", required = false) Integer id, HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {
            if (id != null) {

                boolean inUse = false;
                for (Termin termin : terminService.findAll()) {
                    if (termin.getSalaid() == id) {
                        inUse = true;
                        break;
                    }
                }

                if (inUse) {
                    String msg = "Greska - u toj sali je zakazan trening - that gym is occupied";
                    ModelAndView err = new ModelAndView("sale");
                    err.addObject("msg", msg);
                    err.addObject("sale", salaService.findAll());
                    return err;
                }

                ModelAndView page = new ModelAndView("izmeniSalu");
                page.addObject("sala", salaService.findSalaById(id));
                return page;
            }
        }
        return new ModelAndView("greska");
    }

    @PostMapping(value = "Admin/ObrisiSalu")
    public ModelAndView obrisiPost(@RequestParam(name = "id", required = false) Integer id, HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {
            if (id != null) {

                boolean inUse = false;
                for (Termin termin : terminService.findAll()) {
                    if (termin.getSalaid() == id) {
                        inUse = true;
                        break;
                    }
                }

                if (inUse) {
                    String msg = "Greska - u toj sali je zakazan trening - that gym is occupied";
                    ModelAndView err = new ModelAndView("sale");
                    err.addObject("msg", msg);
                    err.addObject("sale", salaService.findAll());
                    return err;
                }

                System.out.println("delete sala");
                ModelAndView page = new ModelAndView("ulogovaniAdmin");
                page.addObject("treninzi", treningService.findAllTrening());
                page.setViewName("redirect:/Admin");
                return page;
            }
        }
        return new ModelAndView("greska");
    }


    @PostMapping(value = "Admin/IzmeniSalu")
    public ModelAndView izmeniPost(@RequestParam(name = "kapacitet") int kapacitet,
                                   @RequestParam(name = "id") Integer id) {

        Sala sala = salaService.findSalaById(id);
        sala.setKapacitet(kapacitet);
        salaService.updateSala(sala);

        ModelAndView page = new ModelAndView("sale");
        page.addObject("sala", sala);
        page.setViewName("redirect:/Admin/Sale");
        return page;
    }

    @GetMapping(value = "/Admin/DodajSalu")
    public ModelAndView dodajGet(HttpSession session) {
        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {
            return new ModelAndView("dodajSalu");
        }
        return new ModelAndView("greska");
    }

    @PostMapping(value = "/Admin/DodajSalu")
    public ModelAndView dodajPost(
            @RequestParam String oznaka,
            @RequestParam int kapacitet,
            HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {

            try {
                Random random = new Random();
                int id = random.nextInt(899) + 100;

                if (oznaka.length() == 0) {
                    throw new Exception("Morate uneti oznaku");
                }
                if (kapacitet < 1) {
                    throw new Exception("Kapacitet ne moze biti manji od 1");
                }

                Sala sala = new Sala(id, oznaka, kapacitet);
                salaService.saveSala(sala);

                ModelAndView page = new ModelAndView("sale");
                page.addObject("sale", salaService.findAll());
                page.setViewName("redirect:/Admin/Sale");

                return page;

            } catch (Exception ex) {
                String msg = ex.getMessage();
                if (Objects.equals(msg, "")) {
                    msg = "Greska - error";
                }

                ModelAndView res = new ModelAndView("dodajSalu");
                res.addObject("msg", msg);

                return res;
            }
        }
        return new ModelAndView("greska");
    }
}
