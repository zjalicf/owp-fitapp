package com.filip.fitapp.Kontroler;

import com.filip.fitapp.Enum.NivoTreninga;
import com.filip.fitapp.Enum.Trener;
import com.filip.fitapp.Enum.VrstaTreninga;
import com.filip.fitapp.Model.Popust;
import com.filip.fitapp.Model.Sala;
import com.filip.fitapp.Model.Termin;
import com.filip.fitapp.Model.Trening;
import com.filip.fitapp.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller
public class TreningKontroler {

    @Autowired
    private TreningService treningService;

    @Autowired
    private TipService tipService;

    @Autowired
    private SalaService salaService;

    @Autowired
    private TerminService terminService;

    @Autowired
    private KomentarService komentarService;

    @Autowired
    private PopustService popustService;

    @Autowired
    private ServletContext servletContext;
    private String bURL;

    @PostConstruct
    public void init() {
        bURL = servletContext.getContextPath();
    }

    @GetMapping(value = "/Admin/Trening")
    public ModelAndView treningAdmin(@RequestParam() int id, HttpSession session) {

        ModelAndView page = new ModelAndView("trening");
        page.addObject("ulogovanAdmin", Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin")));
        page.addObject("trening", treningService.findTreningById(id));
        page.addObject("komentari", komentarService.findAll());

        return page;
    }

    @GetMapping(value = "/Trening")
    public ModelAndView treningClan(@RequestParam() int id,
                                    HttpSession session) {

        ModelAndView page = new ModelAndView("trening");
        boolean allowedToComment = false;
        page.addObject("allowedToComment", allowedToComment);

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanClan"))) {
            page.addObject("ulogovanClan", Boolean.parseBoolean((String) session.getAttribute("ulogovanClan")));
            List<Trening> treninzi = popustService.setPopust();
            Trening trening = null;
            for (Trening tr: treninzi) {
                if (tr.getId() == id) {
                    trening = tr;
                    page.addObject("trening", tr);
                }
            }
            int korId = (int) session.getAttribute("korId");
            page.addObject("termini", terminService.findTerminiForTrening(id, korId));

            Map<Integer, String> salaMap = new HashMap<>();
            for (Sala sala: salaService.findAll()) {
                salaMap.put(sala.getId(), sala.getOznaka());
            }
            page.addObject("sale", salaMap);

            List<Integer> treningIdList = new ArrayList<>();
            for (Termin termin: terminService.findUserTrainings(korId)) {
                treningIdList.add(termin.getTreningid());
            }
            if(treningIdList.contains(trening.getId())) {
                allowedToComment = true;
            }
            page.addObject("allowedToComment", allowedToComment);

        } else {
            page.addObject("trening", treningService.findTreningById(id));
        }
        page.addObject("komentari", komentarService.findApprovedAndTreningId(id));
        return page;
    }

    @GetMapping(value = "Admin/IzmeniTrening")
    public ModelAndView izmeniGet(@RequestParam(name = "id", required = false) Integer id, HttpSession session) {
        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {
            if (id != null) {
                ModelAndView page = new ModelAndView("izmeniTrening");
                page.addObject("tipovi", tipService.findAll());
                page.addObject("trening", treningService.findTreningById(id));
                return page;
            }
        }
        return new ModelAndView("greska");
    }

    @PostMapping(value = "/Admin/IzmeniTrening")
    public ModelAndView izmeniPost(@RequestParam(value = "id", required = false) Integer id,
                                   @RequestParam(value = "img", required = false) MultipartFile file,
                                   @RequestParam(value = "tipIdList", required = false) List<String> tipIdList,
                                   @RequestParam Map<String, String> allParams, HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {
            if (id != null) {
                try {
                    String folder = "FitApp/images/";
                    byte[] bytes = file.getBytes();
                    Path imgPath = Paths.get(folder + file.getOriginalFilename());
                    String fixImgPath = "/" + String.valueOf(imgPath).replaceAll("\\\\","/");

                    Trening trening = treningService.findTreningById(id);

                    if (allParams.get("nazivTreninga").length() == 0) {
                        throw new Exception("Morate uneti naziv");
                    }
                    if (allParams.get("opis").length() == 0) {
                        throw new Exception("Morate uneti opis");
                    }
                    if (allParams.get("tipIdList").length() == 0) {
                        throw new Exception("Morate uneti tip");
                    }
                    if (allParams.get("cenaTreninga") == null) {
                        throw new Exception("Morate uneti cenu");
                    }
                    if (Integer.parseInt(allParams.get("trajanje")) < 1 && Integer.parseInt(allParams.get("trajanje")) > 4) {
                        throw new Exception("Trening mora da traje minimalno 1 sat ne vise od 4 sata!");
                    }

                    trening.setNazivTreninga(allParams.get("nazivTreninga"));
                    trening.setTrener(Trener.valueOf(allParams.get("trener")));
                    trening.setOpis(allParams.get("opis"));
                    trening.setCenaTreninga(Double.parseDouble(allParams.get("cenaTreninga")));
                    trening.setVrstaTreninga(VrstaTreninga.valueOf(allParams.get("vrstaTreninga")));
                    trening.setNivoTreninga(NivoTreninga.valueOf(allParams.get("nivoTreninga")));
                    trening.setTrajanje(Integer.parseInt(allParams.get("trajanje")));

                    if (!file.isEmpty()) {
                        trening.setImgPath(fixImgPath);
                        Files.write(Path.of("src/main/resources/static/images/" + file.getOriginalFilename()), bytes);
                    }

                    treningService.updateTrening(trening);
                    tipService.updateTreningTip(id, tipIdList);

                    ModelAndView page = new ModelAndView("ulogovaniAdmin");
                    page.addObject("trening", trening);
                    page.addObject("tipovi", tipService.findAll());
                    page.setViewName("redirect:/Admin");
                    return page;

                } catch (Exception ex) {
                    String msg = ex.getMessage();
                    if (Objects.equals(msg, "")) {
                        msg = "Greska - error";
                    }

                    ModelAndView page = new ModelAndView("izmeniTrening");
                    page.addObject("msg", msg);

                    return page;
                }
            }
        }
        return new ModelAndView("greska");
    }

    @GetMapping(value = "Admin/DodajTrening")
    public ModelAndView dodajGet(HttpSession session) {
        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {
            ModelAndView page = new ModelAndView("dodajTrening");
            page.addObject("tipovi", tipService.findAll());
            return page;
        }
        return new ModelAndView("greska");
    }

    @PostMapping(value = "Admin/DodajTrening")
    public ModelAndView dodajPost(
            @RequestParam("img") MultipartFile file,
                                  @RequestParam String nazivTreninga,
                                  @RequestParam Trener trener,
                                  @RequestParam String opis,
                                  @RequestParam List<String> tipIdList,
                                  @RequestParam double cenaTreninga,
                                  @RequestParam VrstaTreninga vrstaTreninga,
                                  @RequestParam NivoTreninga nivoTreninga,
                                  @RequestParam int trajanje,
                                  HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {

            try {
                Random random = new Random();
                int id = random.nextInt(8999) + 1000;
                String folder = "FitApp/images/";
                byte[] bytes = file.getBytes();
                Path imgPath = Paths.get(folder + file.getOriginalFilename());
                String fixImgPath = "/" + String.valueOf(imgPath).replaceAll("\\\\","/");

                if (nazivTreninga.length() == 0) {
                    throw new Exception("Morate uneti naziv");
                }
                if (opis.length() == 0) {
                    throw new Exception("Morate uneti opis");
                }
                if (cenaTreninga == 0) {
                    throw new Exception("Morate uneti cenu");
                }
                if (trajanje < 1 || trajanje > 4) {
                    throw new Exception("Trening mora da traje minimalno 1 sat ne vise od 4 sata!");
                }

                Trening trening = new Trening(id, nazivTreninga, trener, opis, fixImgPath, cenaTreninga,
                vrstaTreninga, nivoTreninga, trajanje, 0);

                treningService.saveTrening(trening);
                tipService.updateTreningTip(id, tipIdList);

                Files.write(Path.of("src/main/resources/static/images/" + file.getOriginalFilename()), bytes);

                ModelAndView page = new ModelAndView("ulogovaniAdmin");
                page.addObject("treninzi", treningService.findAllTrening());
                page.addObject("tipovi", tipService.findAll());

                return page;

            } catch (Exception ex) {
                String msg = ex.getMessage();
                if (Objects.equals(msg, "")) {
                    msg = "Greska - error";
                }

                ModelAndView res = new ModelAndView("dodajTrening");
                res.addObject("msg", msg);

                return res;
            }
        }
        return new ModelAndView("greska");
    }

    @GetMapping(value = "/Admin/Popusti")
    public ModelAndView popustGet(HttpSession session) {

        if (Boolean.parseBoolean((String) session.getAttribute("ulogovanAdmin"))) {
            ModelAndView page = new ModelAndView("popusti");
            page.addObject("treninzi", treningService.findAllTrening());
            return page;
        } else {
            return new ModelAndView("greska");
        }
    }

    @PostMapping(value = "/Admin/Popusti")
    public ModelAndView popustPost(@RequestParam("datumPopusta") String datum,
                                  @RequestParam("procenat") Integer procenat,
                                  @RequestParam(value = "treningIdDropdown", required = false) Integer treningId,
                                  HttpSession session) throws Exception {

        Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(datum);
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = formatter.format(date);
        Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(strDate);

        if (date1.before(date2)) {
            System.out.println("ok");
        }
        if (date1.before(date2) || danasnjiDanBool(date1, date2)) {
            ModelAndView err = new ModelAndView("popusti");
            err.addObject("treninzi", treningService.findAllTrening());
            err.addObject("msg", "Ne mozete dodati popust u proslosti - cant add in past");
            return err;
        }
        if (treningId == null) {
            Popust popust = popustService.findGlobalPopust();
            if (popust.getDatum().getTime() != date1.getTime()) {

                metoda(datum, procenat, 0);

                ModelAndView page = new ModelAndView("ulogovaniAdmin");
                page.addObject("treninzi", treningService.findAllTrening());
                page.addObject("tipovi", tipService.findAll());
                page.addObject("msg", "uspesno dodat popust - added discount");
                return page;
            } else {

                ModelAndView err = new ModelAndView("popusti");
                err.addObject("treninzi", treningService.findAllTrening());
                err.addObject("msg", "Vec postoji popust za taj trening tog datuma - discount defined for that date");
                return err;
            }

        } else {
            for (Popust popust1 : popustService.findAllPopustForTr()) {
                if (popust1.getDatum() == date1 && treningId == popust1.getTreningId()) {

                    ModelAndView err = new ModelAndView("popusti");
                    err.addObject("treninzi", treningService.findAllTrening());
                    err.addObject("msg", "Vec postoji popust za taj trening tog datuma - discount defined for that date");
                    return err;
                }
            }
        }
        return null;
    }

    private void metoda(String datum, int procenat, int treningId) throws ParseException {

        Random random = new Random();
        int id = random.nextInt(8999) + 1000;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date datumFix = format.parse(datum);

        Popust popust = new Popust(id, datumFix, procenat, treningId);
        popustService.savePopust(popust);
    }

    public boolean danasnjiDanBool(Date t, Date d){
        int tDay = t.getDate();
        int tMonth = t.getMonth();
        int tYear = t.getYear();
        int dDay = d.getDate();
        int dMonth = d.getMonth();
        int dYear = d.getYear();

        if(tDay == dDay && tMonth == dMonth && tYear == dYear){
            return true;
        }
        return false;
    }

    @RequestMapping(value = "/Admin/DodajTermin", method = RequestMethod.GET)
    public ModelAndView dodajTerminGet() {

        ModelAndView page= new ModelAndView("dodajTermin");
        page.addObject("treninzi", treningService.findAllTrening());
        page.addObject("sale", salaService.findAll());
        return page;
    }

    @RequestMapping(value = "/Admin/DodajTermin", method = RequestMethod.POST)
    public ModelAndView dodajTerminPost(@RequestParam(name = "treningIdDropdown") Integer idTrening,
                                        @RequestParam(name = "salaDropdown") Integer idSala,
                                        @RequestParam(name = "datum") String datum,
                                        HttpSession session) throws ParseException {

        Random random = new Random();
        int id = random.nextInt(899999) + 100000;

        Date date1 = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm").parse(datum);
        Termin termin = new Termin(id, idSala, idTrening, date1, 0);

        boolean preklapanje = false;

        for (Termin t1: terminService.findAll()) {

            Calendar cal = Calendar.getInstance();
            cal.setTime(t1.getDatum());
            cal.add(Calendar.HOUR_OF_DAY, treningService.findTreningById(t1.getTreningid()).getTrajanje());
            Date kraj1 = cal.getTime();

            Calendar cal2= Calendar.getInstance();
            cal2.setTime(termin.getDatum());
            cal2.add(Calendar.HOUR_OF_DAY, treningService.findTreningById(termin.getTreningid()).getTrajanje());
            Date kraj2 = cal2.getTime();

            if (termin.getDatum().before(kraj1) && t1.getDatum().before(kraj2)) {
                preklapanje = true;
            }
        }

        if (preklapanje) {

            ModelAndView page = new ModelAndView("dodajTermin");
            page.addObject("treninzi", treningService.findAllTrening());
            page.addObject("sale", salaService.findAll());
            page.addObject("msg", "mozete samo jedan trening u ovo vreme");
            return page;
        }

        ModelAndView page = new ModelAndView("ulogovaniAdmin");
        page.addObject("treninzi", treningService.findAllTrening());
        terminService.addTermin(termin);
        page.setViewName("redirect:/Admin");
        //poruka
        return page;
    }
}
