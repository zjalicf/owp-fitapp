package com.filip.fitapp.Service;

import com.filip.fitapp.DAO.IzvestajDAO;
import com.filip.fitapp.Model.Izvestaj;

import java.text.ParseException;
import java.util.List;

public interface IzvestajService {
    List<Izvestaj> getIzvestaji();

    List<Izvestaj> filterIzvestaji(String datumOd, String datumDo, Integer brZakazanih, Integer cena, String sort, String order) throws ParseException;
}
