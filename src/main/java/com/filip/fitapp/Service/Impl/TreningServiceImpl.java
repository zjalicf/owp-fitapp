package com.filip.fitapp.Service.Impl;

import com.filip.fitapp.DAO.TreningDAO;
import com.filip.fitapp.Enum.NivoTreninga;
import com.filip.fitapp.Enum.Trener;
import com.filip.fitapp.Enum.VrstaTreninga;
import com.filip.fitapp.Model.Trening;
import com.filip.fitapp.Service.TreningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class TreningServiceImpl implements TreningService {

    @Autowired
    private TreningDAO treningDAO;

    @Override
    public List<Trening> findAllTrening() {
        return treningDAO.findAllTrening();
    }

    @Override
    public List<Trening> findTrening(String naziv, Double cenaMin, Double cenaMax, String tip, Trener trener, VrstaTreninga vrsta,
                                     NivoTreninga nivo, String kolona, String smer) {
        return treningDAO.findTrening(naziv, cenaMin, cenaMax, tip, trener, vrsta, nivo, kolona, smer);}

    @Override
    public Trening findTreningById(int id) {
        return treningDAO.findTreningById(id);
    }

    @Override
    public void saveTrening(Trening trening) {
        treningDAO.saveTrening(trening);
    }

    @Override
    public void updateTrening(Trening trening) {treningDAO.updateTrening(trening);}
}
