package com.filip.fitapp.Service.Impl;

import com.filip.fitapp.DAO.PopustDAO;
import com.filip.fitapp.Model.Popust;
import com.filip.fitapp.Model.Trening;
import com.filip.fitapp.Service.PopustService;
import com.filip.fitapp.Service.TreningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PopustServiceImpl implements PopustService {

    @Autowired
    PopustDAO popustDAO;

    @Autowired
    TreningService treningService;

    @Override
    public void savePopust(Popust popust) {
        popustDAO.savePopust(popust);
    }

    @Override
    public Popust findGlobalPopust() {
        return popustDAO.findGlobalPopust();
    }

    @Override
    public List<Popust> findAllPopustForTr() {
        return popustDAO.findAllPopustForTr();
    }

    @Override
    public List<Trening> setPopust() {
        Popust globalniPopust = findGlobalPopust();
        List<Popust> treningPopust = findAllPopustForTr();
        List<Trening> treninzi = treningService.findAllTrening();

        if (globalniPopust != null) {
            for (Trening trening: treninzi) {
                trening.setCenaTreninga
                        (trening.getCenaTreninga() - (trening.getCenaTreninga() * globalniPopust.getProcenat()/100));
            }
        } else if (treningPopust != null) {
            int count = 0;
            for (Trening trening: treninzi) {
                if (trening.getId() == treningPopust.get(count).getTreningId()) {
                    trening.setCenaTreninga
                            (trening.getCenaTreninga() - (trening.getCenaTreninga() * treningPopust.get(count).getProcenat()/100));
                }
            }
        }
        return treninzi;
    }
}
