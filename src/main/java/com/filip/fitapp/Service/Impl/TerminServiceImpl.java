package com.filip.fitapp.Service.Impl;

import com.filip.fitapp.DAO.TerminDAO;
import com.filip.fitapp.Model.Termin;
import com.filip.fitapp.Model.Trening;
import com.filip.fitapp.Service.TerminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TerminServiceImpl implements TerminService {

    @Autowired
    private TerminDAO terminDAO;

    @Override
    public List<Termin> findTerminiForTrening(int idTrening, int idKor) {
        return terminDAO.findTerminiForTrening(idTrening, idKor);
    }

    @Override
    public Termin findTerminById(int id) {
        return terminDAO.findTerminById(id);
    }

    @Override
    public void updateTermin(int id, int br) {
        terminDAO.updateTermin(id, br);
    }

    @Override
    public void addKorisnik(int idKor, int idTermin) {
        terminDAO.addKorisnik(idKor, idTermin);
    }

    @Override
    public List<Termin> findAll() {
        return terminDAO.findAll();
    }

    @Override
    public void addTermin(Termin termin) {
        terminDAO.addKorisnik(termin);
    }

    @Override
    public List<Termin> findUserTrainings(int id) {
        return terminDAO.findUserTrainings(id);
    }
}
