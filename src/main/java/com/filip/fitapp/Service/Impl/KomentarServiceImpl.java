package com.filip.fitapp.Service.Impl;

import com.filip.fitapp.DAO.KomentarDAO;
import com.filip.fitapp.Enum.StatusKomentara;
import com.filip.fitapp.Model.Komentar;
import com.filip.fitapp.Model.Korisnik;
import com.filip.fitapp.Service.KomentarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class KomentarServiceImpl implements KomentarService {

    @Autowired
    private KomentarDAO komentarDAO;

    @Override
    public List<Komentar> findAll() {
        return komentarDAO.findAll();
    }

    @Override
    public List<Komentar> findApprovedAndTreningId(int idTrening){

        return komentarDAO.findAll()
                .stream()
                .filter(kom -> kom.getStatusKomentara().equals(StatusKomentara.ODOBREN) && kom.getTrening().getId() == idTrening )
                .collect(Collectors.toList());

    }

    @Override
    public List<Komentar> findAllPending() {
        return komentarDAO.findAllPending();
    }

    @Override
    public void saveKomentar(Komentar komentar) {
        komentarDAO.saveKomentar(komentar);
    }

    @Override
    public Komentar findKomentarById(int id) {
        return komentarDAO.findKomentarById(id);
    }

    @Override
    public void updateKomentar(Komentar komentar) {
        komentarDAO.updateKomentar(komentar);
    }

    @Override
    public List<Komentar> findAllCommentsForTraining(int id) {
        return komentarDAO.findAllCommentsForTraining(id);
    }
}
