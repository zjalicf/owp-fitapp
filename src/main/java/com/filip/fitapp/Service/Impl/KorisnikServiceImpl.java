package com.filip.fitapp.Service.Impl;

import com.filip.fitapp.DAO.KorisnikDAO;
import com.filip.fitapp.Model.Korisnik;
import com.filip.fitapp.Model.Trening;
import com.filip.fitapp.Service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KorisnikServiceImpl implements KorisnikService {

    @Autowired
    private KorisnikDAO korisnikDAO;

    @Override
    public Korisnik findKorisnik(String korIme, String sifra) {
        return korisnikDAO.findKorisnik(korIme, sifra);
    }

    @Override
    public Korisnik findKorisnikUsername(String korIme) {
        return korisnikDAO.findKorisnikUsername(korIme);
    }

    @Override
    public void saveKorisnik(Korisnik korisnik) {
        korisnikDAO.saveKorisnik(korisnik);
    }

    @Override
    public void updateKorisnik(Korisnik korisnik) {
        korisnikDAO.updateKorisnik(korisnik);
    }

    @Override
    public Korisnik findKorisnikById(Integer id) {
        return korisnikDAO.findKorisnikById(id);
    }

    @Override
    public List<Korisnik> findAll() {
        return korisnikDAO.findAll();
    }

    @Override
    public List<Korisnik> findFilter(String korIme, String uloga, String sort, String order) {
        return korisnikDAO.findFilter(korIme, uloga, sort, order);
    }

}
