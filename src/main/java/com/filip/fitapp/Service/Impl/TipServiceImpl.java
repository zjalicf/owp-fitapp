package com.filip.fitapp.Service.Impl;

import com.filip.fitapp.DAO.TipTreningaDAO;
import com.filip.fitapp.Model.TipTreninga;
import com.filip.fitapp.Service.TipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TipServiceImpl implements TipService {

    @Autowired
    private TipTreningaDAO tipTreningaDAO;

    @Override
    public TipTreninga findTip(int id) {
        return tipTreningaDAO.findTip(id);
    }

    @Override
    public List<TipTreninga> findAll() {
        return tipTreningaDAO.findAll();
    }

    @Override
    public void updateTreningTip(int id, List<String> tipIdList) {
        tipTreningaDAO.updateTreningTip(id, tipIdList);
    }

    public static boolean isExists(String naziv, List<TipTreninga> tipovi) {
        for (TipTreninga tip: tipovi) {
            if (tip.getNaziv().equals(naziv)) {
                return true;
            }
        }
        return false;
    }
}
