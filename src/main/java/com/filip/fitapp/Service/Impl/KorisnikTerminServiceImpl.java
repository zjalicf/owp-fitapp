package com.filip.fitapp.Service.Impl;

import com.filip.fitapp.DAO.KorisnikTerminDAO;
import com.filip.fitapp.Service.KorisnikTerminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KorisnikTerminServiceImpl implements KorisnikTerminService {

    @Autowired
    KorisnikTerminDAO korisnikTerminDAO;

    public void otkaziTermin(int korId, int terminId) {
        korisnikTerminDAO.otkaziTermin(korId, terminId);
    }
}
