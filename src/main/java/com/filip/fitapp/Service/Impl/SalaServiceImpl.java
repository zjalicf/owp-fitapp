package com.filip.fitapp.Service.Impl;

import com.filip.fitapp.DAO.SalaDAO;
import com.filip.fitapp.Model.Sala;
import com.filip.fitapp.Service.SalaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalaServiceImpl implements SalaService {

    @Autowired SalaDAO salaDAO;

    @Override
    public List<Sala> findAll() {
        return salaDAO.findAll();
    }

    @Override
    public List<Sala> findAllFilter(String oznaka, String kolona, String smer) {
        return salaDAO.findAllFilter(oznaka, kolona, smer);
    }

    @Override
    public Sala findSalaById(int id) {
        return salaDAO.findSalaById(id);
    }

    @Override
    public void updateSala(Sala sala) {
        salaDAO.updateSala(sala);
    }

    @Override
    public void saveSala(Sala sala) {
        salaDAO.saveSala(sala);
    }
}
