package com.filip.fitapp.Service.Impl;

import com.filip.fitapp.DAO.WishlistDAO;
import com.filip.fitapp.Model.WishlistItem;
import com.filip.fitapp.Service.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WishlistServiceImpl implements WishlistService {

    @Autowired
    private WishlistDAO wishlistDAO;

    @Override
    public List<WishlistItem> getWishlistByUserId(int id) {
        return wishlistDAO.getWishlistByUserId(id);
    }

    @Override
    public void addItemToWishlist(WishlistItem wishlistItem) {
        wishlistDAO.addItemToWishlist(wishlistItem);
    }

    @Override
    public int getTreningIdFromWishlist(int id) {
        return wishlistDAO.getTreningIdFromWishlist(id);
    }

    @Override
    public void removeItem(int korId, int idTrening) {
        wishlistDAO.removeItem(korId, idTrening);
    }
}
