package com.filip.fitapp.Service.Impl;

import com.filip.fitapp.DAO.CardDAO;
import com.filip.fitapp.Model.Zahtev;
import com.filip.fitapp.Service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardServiceImpl implements CardService {

    @Autowired
    CardDAO cardDAO;

    @Override
    public List<Zahtev> getZahtevi() {
        return cardDAO.getZahtevi();
    }

    @Override
    public void posaljiZahtev(int korId) {
        cardDAO.posaljiZahtev(korId);
    }

    @Override
    public Zahtev findZahtev(int id) {
        return cardDAO.findZahtev(id);
    }

    @Override
    public void updateZahtev(Zahtev zahtev) {
        cardDAO.updateZahtev(zahtev);
    }

    @Override
    public void createCard(int korId) {
        cardDAO.createCard(korId);
    }
}
