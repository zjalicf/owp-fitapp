package com.filip.fitapp.Service.Impl;

import com.filip.fitapp.DAO.IzvestajDAO;
import com.filip.fitapp.Model.Izvestaj;
import com.filip.fitapp.Service.IzvestajService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

@Service
public class IzvestajServiceImpl implements IzvestajService {

    @Autowired
    IzvestajDAO izvestajDAO;

    @Override
    public List<Izvestaj> getIzvestaji() {
        return izvestajDAO.getIzvestaji();
    }

    @Override
    public List<Izvestaj> filterIzvestaji(String datumOd, String datumDo, Integer brZakazanih, Integer cena, String sort, String order) throws ParseException {
        return izvestajDAO.filterIzvestaji(datumOd, datumDo, brZakazanih, cena, sort, order);
    }
}
