package com.filip.fitapp.Service;

import com.filip.fitapp.Model.Popust;
import com.filip.fitapp.Model.Trening;

import java.util.List;

public interface PopustService {
    void savePopust(Popust popust);
    Popust findGlobalPopust();
    List<Popust> findAllPopustForTr();
    List<Trening> setPopust();
}
