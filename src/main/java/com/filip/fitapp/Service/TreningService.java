package com.filip.fitapp.Service;

import com.filip.fitapp.Enum.NivoTreninga;
import com.filip.fitapp.Enum.Trener;
import com.filip.fitapp.Enum.VrstaTreninga;
import com.filip.fitapp.Model.Trening;

import java.io.IOException;
import java.util.List;

public interface TreningService {
    List<Trening> findAllTrening();
    List<Trening> findTrening(String naziv, Double cenaMin, Double cenaMax, String tip, Trener trener, VrstaTreninga vrsta, NivoTreninga nivo,
                              String kolona, String smer);
    Trening findTreningById(int id);
    void saveTrening(Trening trening) throws IOException;

    void updateTrening(Trening trening);
}
