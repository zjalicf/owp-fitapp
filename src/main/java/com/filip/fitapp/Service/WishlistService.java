package com.filip.fitapp.Service;

import com.filip.fitapp.Model.WishlistItem;

import java.util.List;


public interface WishlistService {
    List<WishlistItem> getWishlistByUserId(int id);
    void addItemToWishlist(WishlistItem wishlistItem);
    int getTreningIdFromWishlist(int id);

    void removeItem(int korId, int idTrening);
}
