package com.filip.fitapp.Service;

import com.filip.fitapp.Model.TipTreninga;

import java.util.List;

public interface TipService {
    TipTreninga findTip(int id);
    List<TipTreninga> findAll();

    void updateTreningTip(int id, List<String> tipIdList);
}
