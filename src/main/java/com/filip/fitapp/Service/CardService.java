package com.filip.fitapp.Service;

import com.filip.fitapp.Model.Zahtev;

import java.util.List;

public interface CardService {

    List<Zahtev> getZahtevi();
    void posaljiZahtev(int korId);

    Zahtev findZahtev(int id);

    void updateZahtev(Zahtev zahtev);

    void createCard(int korId);
}
