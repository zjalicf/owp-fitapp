package com.filip.fitapp.Service;

import com.filip.fitapp.Model.Sala;

import java.util.List;

public interface SalaService {
    List<Sala> findAll();

    List<Sala> findAllFilter(String oznaka, String kolona, String smer);

    Sala findSalaById(int id);

    void updateSala(Sala sala);

    void saveSala(Sala sala);
}
