package com.filip.fitapp.Service;

import com.filip.fitapp.Model.Komentar;

import java.util.List;

public interface KomentarService {
    List<Komentar> findAll();
    List<Komentar> findApprovedAndTreningId(int idTrening);
    List<Komentar> findAllPending();
    void saveKomentar(Komentar komentar);

    Komentar findKomentarById(int id);

    void updateKomentar(Komentar komentar);

    List<Komentar> findAllCommentsForTraining(int id);
}
