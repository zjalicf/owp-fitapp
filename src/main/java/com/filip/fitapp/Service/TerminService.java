package com.filip.fitapp.Service;

import com.filip.fitapp.Model.Termin;

import java.util.List;

public interface TerminService {
    List<Termin> findTerminiForTrening(int idTrening, int idKor);
    Termin findTerminById(int idTrening);
    void updateTermin(int id, int br);
    void addKorisnik(int idKor, int idTermin);
    List<Termin> findUserTrainings(int id);
    List<Termin> findAll();

    void addTermin(Termin termin);
}
