package com.filip.fitapp.Service;

import com.filip.fitapp.Model.Korisnik;

import java.util.List;

public interface KorisnikService {
    Korisnik findKorisnik(String korIme, String sifra);
    Korisnik findKorisnikUsername(String korIme);
    void saveKorisnik(Korisnik korisnik);
    void updateKorisnik(Korisnik korisnik);
    Korisnik findKorisnikById(Integer id);

    List<Korisnik> findAll();

    List<Korisnik> findFilter(String korIme, String uloga, String sort, String order);
}
