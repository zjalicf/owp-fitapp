package com.filip.fitapp.Model;

public class Izvestaj {

    private int id;
    private String nazivTreninga;
    private String trener;
    private int brZakazanih;
    private int cena;

    public Izvestaj(int id, String nazivTreninga, String trener, int brZakazanih, int cena) {
        this.id = id;
        this.nazivTreninga = nazivTreninga;
        this.trener = trener;
        this.brZakazanih = brZakazanih;
        this.cena = cena;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazivTreninga() {
        return nazivTreninga;
    }

    public void setNazivTreninga(String nazivTreninga) {
        this.nazivTreninga = nazivTreninga;
    }

    public String getTrener() {
        return trener;
    }

    public void setTrener(String trener) {
        this.trener = trener;
    }

    public int getBrZakazanih() {
        return brZakazanih;
    }

    public void setBrZakazanih(int brZakazanih) {
        this.brZakazanih = brZakazanih;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    @Override
    public String toString() {
        return "Izvestaj{" +
                "id=" + id +
                ", nazivTreninga='" + nazivTreninga + '\'' +
                ", trener='" + trener + '\'' +
                ", brZakazanih=" + brZakazanih +
                ", cena=" + cena +
                '}';
    }
}
