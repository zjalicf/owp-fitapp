package com.filip.fitapp.Model;

public class WishlistItem {
    private final int id;
    private int korisnikId;
    private int treningId;

    public WishlistItem(int id, int korisnikId, int treningId) {
        this.id = id;
        this.korisnikId = korisnikId;
        this.treningId = treningId;
    }

    public int getId() {
        return id;
    }

    public int getKorisnikId() {
        return korisnikId;
    }

    public void setKorisnikId(int korisnikId) {
        this.korisnikId = korisnikId;
    }

    public int getTreningId() {
        return treningId;
    }

    public void setTreningId(int treningId) {
        this.treningId = treningId;
    }

    @Override
    public String toString() {
        return "WishlistItem{" +
                "id=" + id +
                ", korisnikId=" + korisnikId +
                ", treningId=" + treningId +
                '}';
    }
}
