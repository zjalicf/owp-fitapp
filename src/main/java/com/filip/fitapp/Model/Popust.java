package com.filip.fitapp.Model;

import java.util.Date;

public class Popust {
    private int id;
    private Date datum;
    private int procenat;
    private int treningId;

    public Popust(int id, Date datum, int procenat, int treningId) {
        this.id = id;
        this.datum = datum;
        this.procenat = procenat;
        this.treningId = treningId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public int getProcenat() {
        return procenat;
    }

    public void setProcenat(int procenat) {
        this.procenat = procenat;
    }

    public int getTreningId() {
        return treningId;
    }

    public void setTreningId(int treningId) {
        this.treningId = treningId;
    }

    @Override
    public String toString() {
        return "Popust{" +
                "id=" + id +
                ", datum=" + datum +
                ", procenat=" + procenat +
                ", treningId=" + treningId +
                '}';
    }
}
