package com.filip.fitapp.Model;

public class ClanskaKartica {
    private int id;
    private int brojPoena;
    private int korId;

    public ClanskaKartica(int id, int brojPoena, int korId) {
        this.id = id;
        this.brojPoena = brojPoena;
        this.korId = korId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBrojPoena() {
        return brojPoena;
    }

    public void setBrojPoena(int brojPoena) {
        this.brojPoena = brojPoena;
    }

    public int getKorId() {
        return korId;
    }

    public void setKorisnik(int korId) {
        this.korId = korId;
    }

    @Override
    public String toString() {
        return "ClanskaKartica{" +
                "id=" + id +
                ", brojPoena=" + brojPoena +
                ", korId=" + korId +
                '}';
    }
}
