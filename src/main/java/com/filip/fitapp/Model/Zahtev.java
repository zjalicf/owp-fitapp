package com.filip.fitapp.Model;

import com.filip.fitapp.Enum.ZahtevStatus;

public class Zahtev {
    private int id;
    private int korId;
    private ZahtevStatus zahtevStatus;

    public Zahtev(int id, int korId, ZahtevStatus zahtevStatus) {
        this.id = id;
        this.korId = korId;
        this.zahtevStatus = zahtevStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKorId() {
        return korId;
    }

    public void setKorId(int korId) {
        this.korId = korId;
    }

    public ZahtevStatus getZahtevStatus() {
        return zahtevStatus;
    }

    public void setZahtevStatus(ZahtevStatus zahtevStatus) {
        this.zahtevStatus = zahtevStatus;
    }

    @Override
    public String toString() {
        return "Zahtev{" +
                "id=" + id +
                ", korId=" + korId +
                ", zahtevStatus=" + zahtevStatus +
                '}';
    }
}
