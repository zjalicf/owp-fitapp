package com.filip.fitapp.Model;
import com.filip.fitapp.Enum.NivoTreninga;
import com.filip.fitapp.Enum.Trener;
import com.filip.fitapp.Enum.VrstaTreninga;

import java.util.List;

public class Trening {
    private int id;
    private String nazivTreninga;
    private Trener trener;
    private String opis;
    private String imgPath;
    private List<TipTreninga> tipTreninga;
    private double cenaTreninga;
    private VrstaTreninga vrstaTreninga;
    private NivoTreninga nivoTreninga;
    private int trajanje;
    private double ocena;

    public Trening(int id, String nazivTreninga, Trener trener, String opis, String imgPath, double cenaTreninga, VrstaTreninga vrstaTreninga, NivoTreninga nivoTreninga, int trajanje, double ocena) {
        this.id = id;
        this.nazivTreninga = nazivTreninga;
        this.trener = trener;
        this.opis = opis;
        this.imgPath = imgPath;
        this.cenaTreninga = cenaTreninga;
        this.vrstaTreninga = vrstaTreninga;
        this.nivoTreninga = nivoTreninga;
        this.trajanje = trajanje;
        this.ocena = ocena;
    }

    public int getId() {
        return id;
    }

    public String getNazivTreninga() {
        return nazivTreninga;
    }

    public void setNazivTreninga(String nazivTreninga) {
        this.nazivTreninga = nazivTreninga;
    }

    public Trener getTrener() {
        return trener;
    }

    public void setTrener(Trener trener) {
        this.trener = trener;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public List<TipTreninga> getTipTreninga() {
        return tipTreninga;
    }

    public void setTipTreninga(List<TipTreninga> tipTreninga) {
        this.tipTreninga = tipTreninga;
    }

    public String getTipTreningaForPrint(){
        String ret = "";
        for(TipTreninga tempTipTreninga : tipTreninga){
            ret += tempTipTreninga.getNaziv() + ", ";
        }
        if(ret.isEmpty()){
            return "";
        }
        return  ret.substring(0, ret.length() - 2);
    }

    public double getCenaTreninga() {
        return cenaTreninga;
    }

    public void setCenaTreninga(double cenaTreninga) {
        this.cenaTreninga = cenaTreninga;
    }

    public VrstaTreninga getVrstaTreninga() {
        return vrstaTreninga;
    }

    public void setVrstaTreninga(VrstaTreninga vrstaTreninga) {
        this.vrstaTreninga = vrstaTreninga;
    }

    public NivoTreninga getNivoTreninga() {
        return nivoTreninga;
    }

    public void setNivoTreninga(NivoTreninga nivoTreninga) {
        this.nivoTreninga = nivoTreninga;
    }

    public int getTrajanje() {
        return trajanje;
    }

    public void setTrajanje(int trajanje) {
        this.trajanje = trajanje;
    }

    public double getOcena() {
        return ocena;
    }

    public void setOcena(double ocena) {
        this.ocena = ocena;
    }



    @Override
    public String toString() {
        return "Trening{" +
                "id=" + id +
                ", nazivTreninga='" + nazivTreninga + '\'' +
                ", trener=" + trener +
                ", opis='" + opis + '\'' +
                ", imgPath='" + imgPath + '\'' +
                ", tipTreninga=" + tipTreninga +
                ", cenaTreninga=" + cenaTreninga +
                ", vrstaTreninga=" + vrstaTreninga +
                ", nivoTreninga=" + nivoTreninga +
                ", trajanje=" + trajanje +
                ", ocena=" + ocena +
                '}';
    }
}
