package com.filip.fitapp.Model;

import com.filip.fitapp.Enum.Uloga;
import java.util.Date;

public class Korisnik {
    private int id;
    private String korIme;
    private String sifra;
    private String email;
    private String ime;
    private String prezime;
    private Date datumRodjenja;
    private String adresa;
    private int brojTelefona;
    private Date datumReg;
    private Uloga uloga;
    private String isBlokiran;

    public Korisnik(int id, String korIme, String sifra, String email, String ime, String prezime, Date datumRodjenja,
                    String adresa, int brojTelefona, Date datumReg, Uloga uloga, String isBlokiran) {
        this.id = id;
        this.korIme = korIme;
        this.sifra = sifra;
        this.email = email;
        this.ime = ime;
        this.prezime = prezime;
        this.datumRodjenja = datumRodjenja;
        this.adresa = adresa;
        this.brojTelefona = brojTelefona;
        this.datumReg = datumReg;
        this.uloga = uloga;
        this.isBlokiran = isBlokiran;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public String getSifra() {
        return sifra;
    }

    public void setSifra(String sifra) {
        this.sifra = sifra;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public Date getDatumRodjenja() {
        return datumRodjenja;
    }

    public void setDatumRodjenja(Date datumRodjenja) {
        this.datumRodjenja = datumRodjenja;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public int getBrojTelefona() {
        return brojTelefona;
    }

    public void setBrojTelefona(int brojTelefona) {
        this.brojTelefona = brojTelefona;
    }

    public Date getDatumReg() {
        return datumReg;
    }

    public void setDatumReg(Date datumReg) {
        this.datumReg = datumReg;
    }

    public Uloga getUloga() {
        return uloga;
    }

    public void setUloga(Uloga uloga) {
        this.uloga = uloga;
    }

    public String getIsBlokiran() {
        return isBlokiran;
    }

    public void setIsBlokiran(String isBlokiran) {
        this.isBlokiran = isBlokiran;
    }

    public boolean isBlokiran() {
        return ("Y".equals(isBlokiran)) ? true : false;
    }

    public void setBlokiran(boolean blokiran) {
        this.isBlokiran = (blokiran) ?  "Y" : "N";
    }

    @Override
    public String toString() {
        return "Korisnik{" +
                "id=" + id +
                ", korIme='" + korIme + '\'' +
                ", sifra='" + sifra + '\'' +
                ", email='" + email + '\'' +
                ", ime='" + ime + '\'' +
                ", prezime='" + prezime + '\'' +
                ", datumRodjenja=" + datumRodjenja +
                ", adresa='" + adresa + '\'' +
                ", brojTelefona=" + brojTelefona +
                ", datumReg=" + datumReg +
                ", uloga=" + uloga +
                ", isBlokiran=" + isBlokiran +
                '}';
    }
}
