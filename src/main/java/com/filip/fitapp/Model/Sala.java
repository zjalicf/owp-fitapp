package com.filip.fitapp.Model;

public class Sala {
    private int id;
    private String oznaka;
    private int kapacitet;

    public Sala(int id, String oznaka, int kapacitet) {
        this.id = id;
        this.oznaka = oznaka;
        this.kapacitet = kapacitet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOznaka() {
        return oznaka;
    }

    public void setOznaka(String oznaka) {
        this.oznaka = oznaka;
    }

    public int getKapacitet() {
        return kapacitet;
    }

    public void setKapacitet(int kapacitet) {
        this.kapacitet = kapacitet;
    }

    @Override
    public String toString() {
        return "Sala{" +
                "id='" + id + '\'' +
                ", oznaka='" + oznaka + '\'' +
                ", kapacitet=" + kapacitet +
                '}';
    }
}
