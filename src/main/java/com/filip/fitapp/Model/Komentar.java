package com.filip.fitapp.Model;

import com.filip.fitapp.Enum.StatusKomentara;

import java.util.Date;

public class Komentar {

    private int id;
    private String tekst;
    private int ocena;
    private Date datum;
    private Korisnik autor;
    private Trening trening;
    private StatusKomentara statusKomentara;
    private String isAnon;

    public Komentar(int id, String tekst, int ocena, Date datum, Korisnik autor, Trening trening, StatusKomentara statusKomentara, String isAnon) {
        this.id = id;
        this.tekst = tekst;
        this.ocena = ocena;
        this.datum = datum;
        this.autor = autor;
        this.trening = trening;
        this.statusKomentara = statusKomentara;
        this.isAnon = isAnon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTekst() {
        return tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    public int getOcena() {
        return ocena;
    }

    public void setOcena(int ocena) {
        this.ocena = ocena;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Korisnik getAutor() {
        return autor;
    }

    public void setAutor(Korisnik autor) {
        this.autor = autor;
    }

    public Trening getTrening() {
        return trening;
    }

    public void setTrening(Trening trening) {
        this.trening = trening;
    }

    public StatusKomentara getStatusKomentara() {
        return statusKomentara;
    }

    public void setStatusKomentara(StatusKomentara statusKomentara) {
        this.statusKomentara = statusKomentara;
    }

    public String getIsAnon() {
        return isAnon;
    }

    public void setIsAnon(String anon){
        this.isAnon = anon;
    }

    public boolean isAnon() {
        return ("Y".equals(isAnon)) ? true : false;
    }

    public void setAnon(boolean anon) {
        this.isAnon = (anon) ?  "Y" : "N";
    }

    @Override
    public String toString() {
        return "Komentar{" +
                "id=" + id +
                ", tekst='" + tekst + '\'' +
                ", ocena=" + ocena +
                ", datum=" + datum +
                ", autor=" + autor +
                ", trening=" + trening +
                ", statusKomentara=" + statusKomentara +
                ", anon=" + isAnon +
                '}';
    }
}
