package com.filip.fitapp.Model;

import com.filip.fitapp.Service.SalaService;

import java.util.Date;

public class Termin {
    private int id;
    private int salaid;
    private int treningid;
    private Date datum;
    private int popunjen;

    public Termin(int id, int salaid, int treningid, Date datum, int popunjen) {
        this.id = id;
        this.salaid = salaid;
        this.treningid = treningid;
        this.datum = datum;
        this.popunjen = popunjen;
    }

    public Termin() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSalaid() {
        return salaid;
    }

    public void setSalaid(int salaid) {
        this.salaid = salaid;
    }

    public int getTreningid() {
        return treningid;
    }

    public void setTreningid(int treningid) {
        this.treningid = treningid;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public int getPopunjen() {
        return popunjen;
    }

    public void setPopunjen(int popunjen) {
        this.popunjen = popunjen;
    }

    @Override
    public String toString() {
        return "Termin{" +
                "id=" + id +
                ", salaid=" + salaid +
                ", treningid=" + treningid +
                ", datum=" + datum +
                ", popunjen=" + popunjen +
                '}';
    }
}
