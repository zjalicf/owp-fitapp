package com.filip.fitapp.Model;

import java.util.Map;

public class Korpa {
    private int id;
    private int clanId;
    private Map<Termin, Trening> terminTreningMap;

    public Korpa(int id, int clanId, Map<Termin, Trening> terminTreningMap) {
        this.id = id;
        this.clanId = clanId;
        this.terminTreningMap = terminTreningMap;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClanId() {
        return clanId;
    }

    public void setClanId(int clanId) {
        this.clanId = clanId;
    }

    public Map<Termin, Trening> getTerminTreningMap() {
        return terminTreningMap;
    }

    public void setTerminTreningMap(Map<Termin, Trening> terminTreningMap) {
        this.terminTreningMap = terminTreningMap;
    }

    @Override
    public String toString() {
        return "Korpa{" +
                "id=" + id +
                ", clanId=" + clanId +
                ", terminTreningMap=" + terminTreningMap +
                '}';
    }
}
