package com.filip.fitapp.DAO.Impl;

import com.filip.fitapp.DAO.TipTreningaDAO;
import com.filip.fitapp.DAO.TreningDAO;
import com.filip.fitapp.Enum.NivoTreninga;
import com.filip.fitapp.Enum.Trener;
import com.filip.fitapp.Enum.VrstaTreninga;
import com.filip.fitapp.Model.Trening;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class TreningDAOImpl implements TreningDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private TipTreningaDAO tipTreningaDAO;

    private class TreningRowCallBackHandler implements RowCallbackHandler {

        private Map<Integer, Trening> treninzi = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resSet) throws SQLException {

            int index=1;
            int id = resSet.getInt(index++);
            String nazivTreninga = resSet.getString(index++);
            Trener trener = Trener.valueOf(resSet.getString(index++));
            String opis = resSet.getString(index++);
            String imgPath = resSet.getString(index++);
            double cenaTreninga = resSet.getDouble(index++);
            VrstaTreninga vrstaTreninga = VrstaTreninga.valueOf(resSet.getString(index++));
            NivoTreninga nivoTreninga = NivoTreninga.valueOf(resSet.getString(index++));
            int trajanje = resSet.getInt(index++);
            double ocena = resSet.getDouble(index);

            Trening trening = treninzi.get(id);

            if(trening == null) {
                trening = new Trening(id, nazivTreninga, trener, opis, imgPath, cenaTreninga, vrstaTreninga,
                        nivoTreninga, trajanje, ocena);
                treninzi.put(trening.getId(), trening);
            }
        }

        public List<Trening> getTreninzi() {
            return new ArrayList<>(treninzi.values()); //ne moze list
        }

    }

    @Override
    public List<Trening> findAllTrening() {
        String sql =
                "select * from trening left join trening_tip_treninga on " +
                "trening_tip_treninga.idtrening = trening.idtrening " +
                "left join tip_treninga on trening_tip_treninga.idtip = tip_treninga.id " +
                "left join termin on trening.idtrening = termin.treningid " +
                "left join sala on sala.id = termin.salaid " +
                "WHERE sala.kapacitet != termin.popunjen ";
            TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
            jdbcTemplate.query(sql, rowCallbackHandler);

            List<Trening> retList = rowCallbackHandler.getTreninzi();
            for(Trening tempTrening : retList){
                fillTreningWithTipTrening(tempTrening);
            }
            return retList;
    }

    @Override
    public List<Trening> findTrening(String naziv, Double cenaMin, Double cenaMax, String tipovi, Trener trener, VrstaTreninga vrsta,
                                     NivoTreninga nivo, String kolona, String smer) {
        String sql =
        "select * from trening left join trening_tip_treninga on " +
        "trening_tip_treninga.idtrening = trening.idtrening " +
        "left join tip_treninga on trening_tip_treninga.idtip = tip_treninga.id " +
        "left join termin on trening.idtrening = termin.treningid " +
        "left join sala on sala.id = termin.salaid " +
        "WHERE sala.kapacitet != termin.popunjen ";

        String and = " AND ";

        if (naziv != null && naziv.length() != 0) {
            sql += and + "nazivTreninga LIKE '%" + naziv + "%'";
        }

        if (!(cenaMin == null || cenaMin == 0) && !(cenaMax == null || cenaMax == 0)) {
            sql += and + "cenaTreninga BETWEEN " + cenaMin + " AND " + cenaMax;
        } else if (!(cenaMin == null || cenaMin == 0)) {
            sql += and + "cenaTreninga > " + (cenaMin - 1);
        } else if (!(cenaMax == null || cenaMax == 0)) {
            sql += and + "cenaTreninga < " + (cenaMax + 1);
        }

        if (trener != null && String.valueOf(trener).length() !=0) {
            sql += and + "trener LIKE '%" + trener + "%'";
        }

        if (vrsta != null && String.valueOf(vrsta).length() != 0) {
            sql += and + "vrstaTreninga LIKE '%" + vrsta + "%'";
        }

        if (nivo != null && String.valueOf(nivo).length() != 0) {
            sql += and + "nivoTreninga LIKE '%" + nivo + "%'";
        }

        if (tipovi != null && tipovi.length() != 0) {
            sql += and + "tip_treninga.id IN (" + tipovi + ")";
        }

        String order = "";
        if (kolona != null && kolona.length() != 0 && smer != null && smer.length() != 0) {
            order = " ORDER BY " + kolona + " " + smer;
        }

        TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();

        jdbcTemplate.query(sql + order, rowCallbackHandler);

        List<Trening> retList = rowCallbackHandler.getTreninzi();
        for(Trening tempTrening : retList){
            fillTreningWithTipTrening(tempTrening);
        }
        return retList;
    }

    private void fillTreningWithTipTrening(Trening tempTrening) {

        int tempTreningId = tempTrening.getId();
        tempTrening.setTipTreninga(new ArrayList<>());
        final List<Integer> tipTreningaForTrening = new ArrayList<>();
        jdbcTemplate.query("select * from trening_tip_treninga where trening_tip_treninga.idtrening = "+ tempTreningId , new RowCallbackHandler() {
            public void processRow(ResultSet rs) throws SQLException {
                tipTreningaForTrening.add(rs.getInt(3));
            }
        });

        for(Integer tipTreningaId : tipTreningaForTrening){
            if(tipTreningaId != null) {
                tempTrening.getTipTreninga().add(tipTreningaDAO.findTip(tipTreningaId));
            }
        }
    }

    @Override
    public Trening findTreningById(int id) {
        String sql = "select * from trening where idtrening = " + id;
        TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);

        Trening trening = rowCallbackHandler.getTreninzi().get(0);
        fillTreningWithTipTrening(trening);

        return trening;
    }

    @Override
    public void saveTrening(Trening trening) {
        String sql = "INSERT INTO trening (idtrening, nazivTreninga, trener, opis, imgPath, cenaTreninga, vrstaTreninga, " +
                "nivoTreninga, trajanje, ocena) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Object[] args = new Object[] {trening.getId(), trening.getNazivTreninga(), String.valueOf(trening.getTrener()),
                trening.getOpis(), trening.getImgPath(), trening.getCenaTreninga(),
                String.valueOf(trening.getVrstaTreninga()), String.valueOf(trening.getNivoTreninga()),
                trening.getTrajanje(), trening.getOcena()};
        jdbcTemplate.update(sql, args);
    }

    @Override
    public void updateTrening(Trening trening) {
        String sql = "UPDATE trening set nazivTreninga = ?, trener = ?, opis = ?, imgPath = ?, cenaTreninga = ?, " +
                "vrstaTreninga = ?, nivoTreninga = ?, trajanje = ?, ocena = ? " +
                "WHERE idtrening = " + trening.getId();
        Object[] args = new Object[] {trening.getNazivTreninga(), String.valueOf(trening.getTrener()),
                trening.getOpis(), trening.getImgPath(), trening.getCenaTreninga(),
                String.valueOf(trening.getVrstaTreninga()), String.valueOf(trening.getNivoTreninga()),
                trening.getTrajanje(), trening.getOcena()};
        jdbcTemplate.update(sql, args);
    }
}
