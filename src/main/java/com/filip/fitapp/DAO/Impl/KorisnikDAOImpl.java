package com.filip.fitapp.DAO.Impl;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import com.filip.fitapp.DAO.KorisnikDAO;
import com.filip.fitapp.Enum.Uloga;
import com.filip.fitapp.Model.Korisnik;
import com.filip.fitapp.Model.Trening;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

@Repository
public class KorisnikDAOImpl implements KorisnikDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class KorisnikRowCallBackHandler implements RowCallbackHandler {

        private Map<Integer, Korisnik> korisnici = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resSet) throws SQLException {

            int index = 1;
            int id = resSet.getInt(index++);
            String korIme = resSet.getString(index++);
            String sifra = resSet.getString(index++);
            String email = resSet.getString(index++);
            String ime = resSet.getString(index++);
            String prezime = resSet.getString(index++);
            Date datumRodjenja = resSet.getDate(index++);
            String adresa = resSet.getString(index++);
            int brojTelefona = resSet.getInt(index++);
            Date datumReg = resSet.getDate(index++);
            Uloga uloga = Uloga.valueOf(resSet.getString(index++));
            String isBlokiran = resSet.getString(index);

            Korisnik korisnik = korisnici.get(id);

            if (korisnik == null) {
                korisnik = new Korisnik(id, korIme, sifra, email, ime, prezime, datumRodjenja, adresa,
                        brojTelefona, datumReg, uloga, isBlokiran);
                korisnici.put(korisnik.getId(), korisnik);
            }
        }
        public List<Korisnik> getKorisnici() {
            return new ArrayList<>(korisnici.values());
        }
    }

    @Override
    public List<Korisnik> findAll() {

        String sql = "select * from korisnik";
        KorisnikDAOImpl.KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikDAOImpl.KorisnikRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getKorisnici();
    }

    @Override
    public Korisnik findKorisnikById(Integer id) {

        String sql = "select * from korisnik where id =" + id;
        KorisnikDAOImpl.KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikDAOImpl.KorisnikRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        if (rowCallbackHandler.getKorisnici().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getKorisnici().get(0);
    }

    @Override
    public Korisnik findKorisnik(String korIme, String sifra) {

        String sql = "SELECT id, korIme, sifra, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumReg, " +
                "uloga, isBlokiran FROM korisnik WHERE korIme ='" + korIme + "' AND sifra ='" + sifra + "'";
        KorisnikDAOImpl.KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikDAOImpl.KorisnikRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        if (rowCallbackHandler.getKorisnici().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getKorisnici().get(0);
    }

    @Override
    public Korisnik findKorisnikUsername(String korIme) {

        String sql = "SELECT id, korIme, sifra, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumReg, " +
                "uloga, isBlokiran FROM korisnik WHERE korIme ='" + korIme + "'";
        KorisnikDAOImpl.KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikDAOImpl.KorisnikRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        if (rowCallbackHandler.getKorisnici().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getKorisnici().get(0);
    }

    @Override
    public void saveKorisnik(Korisnik korisnik) {
            String sql = "INSERT INTO korisnik (id, korIme, sifra, email, ime, prezime, datumRodjenja, adresa, " +
                         "brojTelefona, datumReg, uloga, isBlokiran) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            Object[] args = new Object[] {korisnik.getId(), korisnik.getKorIme(), korisnik.getSifra(), korisnik.getEmail(),
                    korisnik.getIme(), korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(),
                    korisnik.getBrojTelefona(), korisnik.getDatumReg(), String.valueOf(korisnik.getUloga()),
                    korisnik.getIsBlokiran()};
            jdbcTemplate.update(sql, args);
    }

    @Override
    public void updateKorisnik(Korisnik korisnik) {
        String sql = "UPDATE korisnik SET id = ?, korIme = ?, sifra = ?, email = ?, ime = ?, prezime = ?, datumRodjenja = ?, " +
                "adresa = ?, brojTelefona = ?, datumReg = ?, uloga = ?, isBlokiran = ? WHERE id=" + korisnik.getId();
        Object[] args = new Object[] {korisnik.getId(), korisnik.getKorIme(), korisnik.getSifra(), korisnik.getEmail(),
                korisnik.getIme(), korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(),
                korisnik.getBrojTelefona(), korisnik.getDatumReg(), String.valueOf(korisnik.getUloga()),
                korisnik.getIsBlokiran()};
        jdbcTemplate.update(sql, args);
    }

    @Override
    public List<Korisnik> findFilter(String korIme, String uloga, String sort, String order) {

        String sql = "select * from korisnik WHERE 1=1";

        String and = " AND ";

        if (korIme != null && korIme.length() != 0) {
            sql += and + "korIme LIKE '%" + korIme + "%'";
        }

        if (uloga != null && uloga.length() !=0) {
            sql += and + "uloga LIKE '%" + uloga + "%'";
        }

        String orderBy = "";
        if (sort != null && sort.length() != 0 && order != null && order.length() != 0) {
            orderBy = " ORDER BY " + sort + " " + order;
        }

        KorisnikDAOImpl.KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikDAOImpl.KorisnikRowCallBackHandler();

        jdbcTemplate.query(sql + orderBy, rowCallbackHandler);

        return rowCallbackHandler.getKorisnici();
    }
}
