package com.filip.fitapp.DAO.Impl;

import com.filip.fitapp.DAO.CardDAO;
import com.filip.fitapp.Enum.StatusKomentara;
import com.filip.fitapp.Enum.ZahtevStatus;
import com.filip.fitapp.Model.Zahtev;
import com.filip.fitapp.Model.Zahtev;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class CardDAOImpl implements CardDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private class ZahtevRowCallBackHandler implements RowCallbackHandler {

        private Map<Integer, Zahtev> zahtevi = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resSet) throws SQLException {

            int index = 1;
            int id = resSet.getInt(index++);
            int korId = resSet.getInt(index++);
            ZahtevStatus status = ZahtevStatus.valueOf(resSet.getString(index));

            Zahtev zahtev = zahtevi.get(id);

            if (zahtev == null) {
                zahtev = new Zahtev(id, korId, status);
                zahtevi.put(zahtev.getId(), zahtev);
            }
        }
        public List<Zahtev> getZah() {
            return new ArrayList<>(zahtevi.values());
        }
    }

    @Override
    public List<Zahtev> getZahtevi() {
        String sql ="select * from zahtev where status ='" + ZahtevStatus.NA_CEKANJU + "'";
        CardDAOImpl.ZahtevRowCallBackHandler rowCallbackHandler = new CardDAOImpl.ZahtevRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getZah();
    }

    @Override
    public void posaljiZahtev(int korId) {
        Random random = new Random();
        int id = random.nextInt(8999) + 1000;
        String sql = "insert into zahtev values (" + id + ", " + korId + ", '" + ZahtevStatus.NA_CEKANJU + "')";
        jdbcTemplate.update(sql);
    }

    @Override
    public Zahtev findZahtev(int id) {

        String sql = "select * from zahtev where id=" + id;
        CardDAOImpl.ZahtevRowCallBackHandler rowCallbackHandler = new CardDAOImpl.ZahtevRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getZah().get(0);
    }

    @Override
    public void updateZahtev(Zahtev zahtev) {
        String sql = "update zahtev set status = '" + zahtev.getZahtevStatus() + "' where id=" + zahtev.getId();
        jdbcTemplate.update(sql);
    }

    @Override
    public void createCard(int korId) {
        Random random = new Random();
        int id = random.nextInt(8999) + 1000;
        String sql = "insert into card values (" + id + ", " + 10 + ", " + korId + ")";
        jdbcTemplate.update(sql);
    }
}
