package com.filip.fitapp.DAO.Impl;

import com.filip.fitapp.DAO.IzvestajDAO;
import com.filip.fitapp.Enum.StatusKomentara;
import com.filip.fitapp.Model.Izvestaj;
import com.filip.fitapp.Model.Komentar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class IzvestajDAOImpl implements IzvestajDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class IzvestajRowCallBackHandler implements RowCallbackHandler {

        private Map<Integer, Izvestaj> izvestaji = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resSet) throws SQLException {

            int index = 1;
            int id = resSet.getInt(index++);
            String nazivTreninga = resSet.getString(index++);
            String trener = resSet.getString(index++);
            int broj = resSet.getInt(index++);
            int cena = resSet.getInt(index);

            Izvestaj izvestaj = izvestaji.get(id);

            if (izvestaj == null) {
                izvestaj = new Izvestaj(id, nazivTreninga, trener, broj, cena);
                izvestaji.put(izvestaj.getId(), izvestaj);
            }
        }
        public List<Izvestaj> getIzv() {
            return new ArrayList<>(izvestaji.values());
        }
    }

    @Override
    public List<Izvestaj> getIzvestaji() {

        String sql = "select ROW_NUMBER() OVER(order by nazivTreninga) as id, trening.nazivTreninga, trening.trener, count(korisnik_termin.idTermin), sum(trening.cenaTreninga)" +
                " from trening, korisnik_termin, termin" +
                " where trening.idtrening = termin.treningid AND korisnik_termin.idTermin = termin.id" +
                " group by nazivTreninga";

        IzvestajDAOImpl.IzvestajRowCallBackHandler rowCallbackHandler = new IzvestajDAOImpl.IzvestajRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getIzv();
    }

    @Override
    public List<Izvestaj> filterIzvestaji(String datumOd, String datumDo, Integer brZakazanih, Integer cena, String kolona, String smer) throws ParseException {

        Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(datumOd);
        Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(datumDo);

        String sql = "select ROW_NUMBER() OVER(order by nazivTreninga) as id, trening.nazivTreninga, trening.trener, count(korisnik_termin.idTermin), sum(trening.cenaTreninga)" +
                " from trening, korisnik_termin, termin" +
                " where trening.idtrening = termin.treningid AND korisnik_termin.idTermin = termin.id and termin.datum between '" + datumOd + "' and '" + datumDo +
                "' group by nazivTreninga";

        String order = "";
        if (kolona != null && kolona.length() != 0 && smer != null && smer.length() != 0) {
            order = " ORDER BY " + kolona + " " + smer;
        }
        IzvestajDAOImpl.IzvestajRowCallBackHandler rowCallbackHandler = new IzvestajDAOImpl.IzvestajRowCallBackHandler();
        jdbcTemplate.query(sql + order, rowCallbackHandler);
        return rowCallbackHandler.getIzv();
    }
}
