package com.filip.fitapp.DAO.Impl;

import com.filip.fitapp.DAO.PopustDAO;
import com.filip.fitapp.Model.Popust;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class PopustDAOImpl implements PopustDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class PopustRowCallBackHandler implements RowCallbackHandler {

        private Map<Integer, Popust> popusti = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resSet) throws SQLException {

            int index = 1;
            int id = resSet.getInt(index++);
            Date datum = resSet.getDate(index++);
            int procenat = resSet.getInt(index++);
            int treningId = resSet.getInt(index);

            Popust popust = popusti.get(id);

            if (popust == null) {
                popust = new Popust(id, datum, procenat, treningId);
                popusti.put(popust.getId(), popust);
            }
        }
        public List<Popust> getPopusti() {
            return new ArrayList<>(popusti.values());
        }
    }


    @Override
    public void savePopust(Popust popust) {
        String sql = "insert into popust values (?,?,?,?)";
        Object[] args = new Object[] {popust.getId(), popust.getDatum(), popust.getProcenat(), popust.getTreningId()};
        jdbcTemplate.update(sql, args);
    }

    @Override
    public Popust findGlobalPopust() {
        String sql = "select * from popust inner join trening on popust.treningid = 0 where datum = curdate()";
        PopustDAOImpl.PopustRowCallBackHandler rowCallbackHandler = new PopustDAOImpl.PopustRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        List<Popust> retList = rowCallbackHandler.getPopusti();
        if (retList.isEmpty()) {
            return null;
        }
        return rowCallbackHandler.getPopusti().get(0);
    }

    @Override
    public List<Popust> findAllPopustForTr() {
        String sql = "select * from popust inner join trening on datum = curdate() and treningid = idtrening";
        PopustDAOImpl.PopustRowCallBackHandler rowCallbackHandler = new PopustDAOImpl.PopustRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        List<Popust> retList = rowCallbackHandler.getPopusti();
        if (retList.isEmpty()) {
            return null;
        }
        return rowCallbackHandler.getPopusti();
    }
}
