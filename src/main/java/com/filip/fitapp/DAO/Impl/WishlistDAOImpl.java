package com.filip.fitapp.DAO.Impl;

import com.filip.fitapp.DAO.WishlistDAO;
import com.filip.fitapp.Model.WishlistItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class WishlistDAOImpl implements WishlistDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class WishlistRowCallBackHandler implements RowCallbackHandler {

        private Map<Integer, WishlistItem> wishlist = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resSet) throws SQLException {

            int index = 1;
            int id = resSet.getInt(index++);
            int korisnikId = resSet.getInt(index++);
            int treningId = resSet.getInt(index);

            WishlistItem wishlistItem = wishlist.get(id);

            if (wishlistItem == null) {
                wishlistItem = new WishlistItem(id, korisnikId, treningId);
                wishlist.put(wishlistItem.getId(), wishlistItem);
            }
        }
        public List<WishlistItem> getWishlist() {
            return new ArrayList<>(wishlist.values());
        }
    }

    @Override
    public List<WishlistItem> getWishlistByUserId(int id) {
        String sql = "select * from wishlist where idkorisnik = " + id;
        WishlistDAOImpl.WishlistRowCallBackHandler rowCallbackHandler = new WishlistDAOImpl.WishlistRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);

        return rowCallbackHandler.getWishlist();
    }

    @Override
    public void addItemToWishlist(WishlistItem wishlistItem) {
        String sql = "insert into wishlist values (" + wishlistItem.getId() + ", " + wishlistItem.getKorisnikId() + ", " + wishlistItem.getTreningId() + ")";
        jdbcTemplate.update(sql);
    }

    @Override
    public int getTreningIdFromWishlist(int id) {
        String sql = "select idtrening from wishlist where id = " + id;
        WishlistDAOImpl.WishlistRowCallBackHandler rowCallbackHandler = new WishlistDAOImpl.WishlistRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getWishlist().get(0).getTreningId();
    }

    @Override
    public void removeItem(int korId, int idTrening) {
        String sql = "delete wishlist from wishlist where idkorisnik = " + korId + " and idtrening = " + idTrening;
        jdbcTemplate.update(sql);
    }
}
