package com.filip.fitapp.DAO.Impl;

import com.filip.fitapp.DAO.TipTreningaDAO;
import com.filip.fitapp.Model.TipTreninga;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;

@Repository
public class TipTreningaDAOImpl implements TipTreningaDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class TipTreningaRowMapper implements RowMapper<TipTreninga> {

        @Override
        public TipTreninga mapRow(ResultSet rs, int rowNum) throws SQLException {
            int index = 1;
            int id = rs.getInt(index++);
            String naziv = rs.getString(index++);
            String opis = rs.getString(index);

            return new TipTreninga(id, naziv, opis);
        }
    }

    @Override
    public TipTreninga findTip(int id) {
        String sql = "SELECT id, naziv, opis FROM tip_treninga WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new TipTreningaRowMapper(), id);
    }

    @Override
    public List<TipTreninga> findAll() {
        String sql = "SELECT id, naziv, opis FROM tip_treninga";
        return jdbcTemplate.query(sql, new TipTreningaRowMapper());
    }

    @Override
    public void updateTreningTip(int id, List<String> tipIdList) {
        for (String tipId: tipIdList) {
            Random random = new Random();
            int idRandom = random.nextInt(899) + 100;
            String sql = "INSERT INTO trening_tip_treninga(id, idtrening, idtip) values (?, ?, ?)";
            Object[] args = new Object[] {idRandom, id, tipId};
            jdbcTemplate.update(sql, args);
        }
    }
}
