package com.filip.fitapp.DAO.Impl;

import com.filip.fitapp.DAO.KomentarDAO;
import com.filip.fitapp.Enum.StatusKomentara;
import com.filip.fitapp.Enum.Uloga;
import com.filip.fitapp.Model.Komentar;
import com.filip.fitapp.Model.Sala;
import com.filip.fitapp.Service.KorisnikService;
import com.filip.fitapp.Service.TreningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class KomentarDAOImpl implements KomentarDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private TreningService treningService;

    @Autowired
    private KorisnikService korisnikService;

    private class KomentarRowCallBackHandler implements RowCallbackHandler {

        private Map<Integer, Komentar> komentari = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resSet) throws SQLException {

            int index = 1;
            int id = resSet.getInt(index++);
            String tekst = resSet.getString(index++);
            int ocena = resSet.getInt(index++);
            Date datum = resSet.getDate(index++);
            int korId = resSet.getInt(index++);
            int treningId = resSet.getInt(index++);
            StatusKomentara status = StatusKomentara.valueOf(resSet.getString(index++));
            String anon =  resSet.getString(index);

            Komentar komentar = komentari.get(id);

            if (komentar == null) {
                komentar = new Komentar(id, tekst, ocena, datum, korisnikService.findKorisnikById(korId),
                        treningService.findTreningById(treningId), status, anon);
                komentari.put(komentar.getId(), komentar);
            }
        }
        public List<Komentar> getKomentari() {
            return new ArrayList<>(komentari.values());
        }
    }

    public List<Komentar> findAll() {
        String sql = "select * from komentar";
        KomentarDAOImpl.KomentarRowCallBackHandler rowCallbackHandler = new KomentarDAOImpl.KomentarRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getKomentari();
    }

    public List<Komentar> findAllPending() {
        String sql = "select * from komentar where status= 'NA_CEKANJU'";
        KomentarDAOImpl.KomentarRowCallBackHandler rowCallbackHandler = new KomentarDAOImpl.KomentarRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getKomentari();
    }

    public void saveKomentar(Komentar komentar) {
        String sql = "insert into komentar values (?, ?, ?, ?, ?, ?, ?, ?)";
        Object[] args = new Object[] {komentar.getId(), komentar.getTekst(), komentar.getOcena(), komentar.getDatum(),
                komentar.getAutor().getId(), komentar.getTrening().getId(), String.valueOf(komentar.getStatusKomentara()), komentar.getIsAnon()};
        jdbcTemplate.update(sql, args);
    }

    @Override
    public Komentar findKomentarById(int id) {
        String sql = "select * from komentar where id =" + id;
        KomentarDAOImpl.KomentarRowCallBackHandler rowCallbackHandler = new KomentarDAOImpl.KomentarRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getKomentari().get(0);
    }

    @Override
    public void updateKomentar(Komentar komentar) {
        String sql = "update komentar set status='" + komentar.getStatusKomentara() + "' where id=" + komentar.getId();
        jdbcTemplate.update(sql);
    }

    @Override
    public List<Komentar> findAllCommentsForTraining(int id) {
        String sql = "select * from komentar where idtrening =" + id + " and status='" + StatusKomentara.ODOBREN + "'";
        KomentarDAOImpl.KomentarRowCallBackHandler rowCallbackHandler = new KomentarDAOImpl.KomentarRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getKomentari();
    }
}
