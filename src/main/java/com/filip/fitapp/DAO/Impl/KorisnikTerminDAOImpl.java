package com.filip.fitapp.DAO.Impl;

import com.filip.fitapp.DAO.KorisnikTerminDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class KorisnikTerminDAOImpl implements KorisnikTerminDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void otkaziTermin(int korId, int terminId) {
        String sql = "delete korisnik_termin from korisnik_termin where idKorisnik=" + korId + " and idTermin=" + terminId;
        jdbcTemplate.update(sql);
    }
}
