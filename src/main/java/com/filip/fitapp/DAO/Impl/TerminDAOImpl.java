package com.filip.fitapp.DAO.Impl;

import com.filip.fitapp.DAO.TerminDAO;
import com.filip.fitapp.Model.Termin;
import com.filip.fitapp.Model.Trening;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Repository
public class TerminDAOImpl implements TerminDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class TerminRowCallBackHandler implements RowCallbackHandler {

        private Map<Integer, Termin> termini = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resSet) throws SQLException {

            int index = 1;
            int id = resSet.getInt(index++);
            int salaid = resSet.getInt(index++);
            int treningid = resSet.getInt(index++);
            LocalDateTime datum = resSet.getObject(index++, LocalDateTime.class);
            Instant instant = datum.atZone(ZoneId.systemDefault()).toInstant();
            Date datumFix = Date.from(instant);
            int popunjen = resSet.getInt(index);

            Termin termin = termini.get(id);

            if (termin == null) {
                termin = new Termin(id, salaid, treningid, datumFix, popunjen);
                termini.put(termin.getId(), termin);
            }
        }
        public List<Termin> getTermini() {
            return new ArrayList<>(termini.values());
        }
    }

    @Override
    public List<Termin> findTerminiForTrening(int idTrening, int idKor) {
        String sql = "select * from termin " +
                "left join sala on termin.salaid = sala.id " +
                "left join korisnik_termin on termin.id = korisnik_termin.idtermin " +
                "where popunjen != kapacitet and (idkorisnik != " + idKor + " or idKorisnik is null) " +
                "and treningid =" + idTrening;
        TerminRowCallBackHandler rowCallbackHandler = new TerminRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);

        return rowCallbackHandler.getTermini();
    }

    @Override
    public Termin findTerminById(int id) {
        String sql = "select * from termin where id = " + id;
        TerminRowCallBackHandler rowCallBackHandler = new TerminRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallBackHandler);

        return rowCallBackHandler.getTermini().get(0);
    }

    @Override
    public void updateTermin(int id, int br) {
        String sql = "UPDATE termin SET popunjen = " + br + " WHERE id = " + id;
        jdbcTemplate.update(sql);
    }

    @Override
    public void addKorisnik(int idKor, int idTermin) {
        String sql = "INSERT INTO korisnik_termin values (" + idKor + ", " + idTermin + ")";
        jdbcTemplate.update(sql);
    }

    @Override
    public List<Termin> findAll() {
        String sql = "SELECT * from termin";
        TerminRowCallBackHandler rowCallBackHandler = new TerminRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallBackHandler);
        return rowCallBackHandler.getTermini();
    }

    @Override
    public void addKorisnik(Termin termin) {
        String sql = "insert into termin values (?,?,?,?,?)";
        Object[] args = new Object[] {termin.getId(), termin.getSalaid(), termin.getTreningid(), termin.getDatum(), termin.getPopunjen()};
        jdbcTemplate.update(sql, args);
    }

    @Override
    public List<Termin> findUserTrainings(int id) {
        String sql = "SELECT * FROM termin left join korisnik_termin on korisnik_termin.idTermin = id " +
                "where idKorisnik = " + id + " order by datum desc";
        TerminRowCallBackHandler rowCallBackHandler = new TerminRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallBackHandler);
        return rowCallBackHandler.getTermini();
    }
}
