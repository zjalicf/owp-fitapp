package com.filip.fitapp.DAO.Impl;

import com.filip.fitapp.DAO.SalaDAO;
import com.filip.fitapp.Model.Sala;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class SalaDAOImpl implements SalaDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class SalaRowCallBackHandler implements RowCallbackHandler {

        private Map<Integer, Sala> sale = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resSet) throws SQLException {

            int index = 1;
            int id = resSet.getInt(index++);
            String oznaka = resSet.getString(index++);
            int kapacitet = resSet.getInt(index);

            Sala sala = sale.get(id);

            if (sala == null) {
                sala = new Sala(id, oznaka, kapacitet);
                sale.put(sala.getId(), sala);
            }
        }
        public List<Sala> getSale() {
            return new ArrayList<>(sale.values());
        }
    }

    public List<Sala> findAll() {
        String sql = "select * from sala";
        SalaDAOImpl.SalaRowCallBackHandler rowCallbackHandler = new SalaDAOImpl.SalaRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);

        return rowCallbackHandler.getSale();
    }

    public List<Sala> findAllFilter(String oznaka, String kolona, String smer) {
        if (oznaka==null) {
            oznaka="";
        }
        String sql = "select * from sala WHERE oznaka LIKE '%" + oznaka + "%'";
        String order = "";
        if (kolona != null && kolona.length() != 0 && smer != null && smer.length() != 0) {
            order = " ORDER BY " + kolona + " " + smer;
        }
        SalaDAOImpl.SalaRowCallBackHandler rowCallbackHandler = new SalaDAOImpl.SalaRowCallBackHandler();
        System.out.println(sql);
        jdbcTemplate.query(sql + order, rowCallbackHandler);
        return rowCallbackHandler.getSale();
    }

    public Sala findSalaById(int id) {

        String sql = "select * from sala where id = " + id;
        SalaDAOImpl.SalaRowCallBackHandler rowCallbackHandler = new SalaDAOImpl.SalaRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getSale().get(0);
    }

    @Override
    public void updateSala(Sala sala) {

        String sql = "update sala set kapacitet = " + sala.getKapacitet() + " where id = " + sala.getId();
        jdbcTemplate.update(sql);
    }

    @Override
    public void saveSala(Sala sala) {

        String sql = "insert into sala values (" + sala.getId() + ", '" + sala.getOznaka() + "', " + sala.getKapacitet() + ")";
        jdbcTemplate.update(sql);
    }
}
