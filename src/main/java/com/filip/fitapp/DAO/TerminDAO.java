package com.filip.fitapp.DAO;

import com.filip.fitapp.Model.Termin;

import java.util.List;


public interface TerminDAO {

    Termin findTerminById(int id);
    void updateTermin(int id, int br);
    List<Termin> findTerminiForTrening(int idTrening, int idKor);
    void addKorisnik(int idKor, int idTermin);
    List<Termin> findUserTrainings(int id);
    List<Termin> findAll();

    void addKorisnik(Termin termin);
}
