package com.filip.fitapp.DAO;

import com.filip.fitapp.Model.Komentar;

import java.util.List;

public interface KomentarDAO {

    List<Komentar> findAll();
    void saveKomentar(Komentar komentar);
    Komentar findKomentarById(int id);
    void updateKomentar(Komentar komentar);
    List<Komentar> findAllPending();
    List<Komentar> findAllCommentsForTraining(int id);
}
