package com.filip.fitapp.DAO;

import com.filip.fitapp.Model.WishlistItem;

import java.util.List;

public interface WishlistDAO {

    List<WishlistItem> getWishlistByUserId(int id);
    void addItemToWishlist(WishlistItem wishlistItem);

    int getTreningIdFromWishlist(int id);

    void removeItem(int korId, int idTrening);
}
