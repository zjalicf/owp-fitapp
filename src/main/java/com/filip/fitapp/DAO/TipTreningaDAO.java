package com.filip.fitapp.DAO;

import com.filip.fitapp.Model.TipTreninga;

import java.util.List;

public interface TipTreningaDAO {

    TipTreninga findTip(int id);
    List<TipTreninga> findAll();

    void updateTreningTip(int id, List<String> tipIdList);
}
