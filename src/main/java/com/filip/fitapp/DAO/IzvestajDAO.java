package com.filip.fitapp.DAO;

import com.filip.fitapp.Model.Izvestaj;

import java.text.ParseException;
import java.util.List;

public interface IzvestajDAO {
    List<Izvestaj> getIzvestaji();

    List<Izvestaj> filterIzvestaji(String datumOd, String datumDo, Integer brZakazanih, Integer cena, String sort, String order) throws ParseException;
}
