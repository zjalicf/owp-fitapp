package com.filip.fitapp.DAO;

import com.filip.fitapp.Model.Korisnik;
import com.filip.fitapp.Model.Trening;

import java.util.List;

public interface KorisnikDAO {

    List<Korisnik> findAll();
    Korisnik findKorisnikById(Integer id);
    Korisnik findKorisnik(String korIme, String sifra);
    Korisnik findKorisnikUsername(String KorIme);
    void saveKorisnik(Korisnik korisnik);
    void updateKorisnik(Korisnik korisnik);

    List<Korisnik> findFilter(String korIme, String uloga, String sort, String order);
}
