package com.filip.fitapp.DAO;

import com.filip.fitapp.Model.Popust;

import java.util.List;

public interface PopustDAO {
    void savePopust(Popust popust);

    Popust findGlobalPopust();

    List<Popust> findAllPopustForTr();
}
